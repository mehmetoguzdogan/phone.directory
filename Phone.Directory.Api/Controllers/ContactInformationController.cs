﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Phone.Directory.Repository.Model;
using Phone.Directory.Service.ContactInformations;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Phone.Directory.Api.Controllers
{
    [Route("api/[controller]")]
    public class ContactInformationController : Controller
    {
        private readonly IContactInformationService _contactInformationService;


        public ContactInformationController(IContactInformationService contactInformationService)
        {
            _contactInformationService = contactInformationService;
        }
        // GET: api/values
        [HttpGet]
        public List<ContactInformation> Get()
        {
            return _contactInformationService.GetList();
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public ContactInformation Get(Guid id)
        {
            return _contactInformationService.Get(id);
        }

        // POST api/values
        [HttpPost]
        public Guid Post([FromBody] ContactInformation contact)
        {
            return _contactInformationService.Add(contact);
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public ContactInformation Put(Guid id, [FromBody] ContactInformation contactInformation)
        {
            return _contactInformationService.Update(id, contactInformation);
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public bool Delete(Guid id)
        {
            return _contactInformationService.Delete(id);
        }
    }
}

