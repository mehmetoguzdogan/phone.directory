﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Phone.Directory.Repository.Model;
using Phone.Directory.Service.Persons;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Phone.Directory.Api.Controllers
{
    [Route("api/[controller]")]
    public class PersonController : Controller
    {
        private readonly IPersonService _personService;


        public PersonController(IPersonService personService)
        {
            _personService = personService;
        }
        // GET: api/values
        [HttpGet]
        public List<Person> Get()
        {
            return _personService.GetList();
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public Person Get(Guid id)
        {
            return _personService.Get(id);
        }

        // POST api/values
        [HttpPost]
        public Guid Post([FromBody]Person person)
        {
            return _personService.Add(person);
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public Person Put(Guid id, [FromBody]Person person)
        {
           return _personService.Update(id, person);
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public bool Delete(Guid id)
        {
           return _personService.Delete(id);
        }
    }
}

