﻿using Microsoft.AspNetCore.Mvc;
using Phone.Directory.Repository.Enum;
using Phone.Directory.Repository.Model;
using Phone.Directory.Service.ContactInformations;
using Phone.Directory.Service.Persons;
using Phone.Directory.Service.Report;
using Report.Queuing.Model.QueueModel;

namespace Phone.Directory.Api.Controllers;

[ApiController]
[Route("[controller]")]
public class WeatherForecastController : ControllerBase
{
    private static readonly string[] Summaries = new[]
    {
        "Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching"
    };

    private readonly ILogger<WeatherForecastController> _logger;

    public WeatherForecastController(ILogger<WeatherForecastController> logger)
    {
        _logger = logger;
    }
        [HttpGet]
        public IEnumerable<WeatherForecast> Get()
        {
        return Enumerable.Range(1, 5).Select(index => new WeatherForecast
            {
                Date = DateTime.Now.AddDays(index),
                TemperatureC = Random.Shared.Next(-20, 55),
                Summary = Summaries[Random.Shared.Next(Summaries.Length)]
            })
            .ToArray();
        }
}
