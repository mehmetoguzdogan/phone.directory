﻿using System;
namespace Phone.Directory.Api
{
    public class CustomStartup : Phone.Directory.Infrastructure.Engine.StartupBase, Phone.Directory.Infrastructure.Engine.IStartUp
    {
        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="CustomStartup"/> class.
        /// </summary>
        /// <param name="configuration">The configuration<see cref="IConfiguration"/>.</param>
        public CustomStartup(IConfigurationRoot configuration) : base(configuration)
        {
            Configuration = configuration;
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets the Configuration.
        /// </summary>
        public new IConfigurationRoot Configuration { get; }

        #endregion

        #region Methods

        /// <summary>
        /// The CustomConfigure.
        /// </summary>
        /// <param name="app">The app<see cref="IApplicationBuilder"/>.</param>
        /// <param name="env">The env<see cref="IHostingEnvironment"/>.</param>
        /// <param name="loggerFactory">The loggerFactory<see cref="ILoggerFactory"/>.</param>
        public override void CustomConfigure(IApplicationBuilder app, IHostApplicationLifetime env) { }

        /// <summary>
        /// The CustomConfigureServices.
        /// </summary>
        /// <param name="services">The services<see cref="IServiceCollection"/>.</param>
        public override void CustomConfigureServices(IServiceCollection services) { }

        #endregion
    }
}

