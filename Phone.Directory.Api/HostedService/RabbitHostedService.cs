﻿using System;
using Report.Queuing.Model.QueueModel;

namespace Phone.Directory.Api.HostedService
{
    public class RabbitHostedService : IHostedService
    {
        public Task StartAsync(CancellationToken cancellationToken)
        {
            Task.Run(async () =>
            {
                while (!cancellationToken.IsCancellationRequested)
                {
                    Report.Queuing.Queue.Subscribe<ReportQueueModel>(ReportHandler.ReportPrepare, prefetchCount: 1);
                    await Task.Delay(new TimeSpan(0, 0, 20)); // 20 second delay
                }
            });

            return Task.CompletedTask;
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            return Task.CompletedTask;
        }
    }
}

