﻿using Phone.Directory.Api;
using Phone.Directory.Api.HostedService;
using Phone.Directory.Repository.Engine;
using Report.Queuing.Model.QueueModel;

var builder = WebApplication.CreateBuilder(args);


var startup = new CustomStartup(builder.Configuration);
startup.ConfigureServices(builder.Services);
// Add services to the container.

builder.Services.AddHostedService<RabbitHostedService>();
builder.Services.AddControllers();

var app = builder.Build();


// Configure the HTTP request pipeline.

app.UseAuthorization();

app.MapControllers();
FakeContext.SetServiceProvider(app.Services);

app.Run();





