﻿using System;
using Phone.Directory.Repository.Engine;
using Phone.Directory.Service.Report;
using Phone.Directory.Service.Report.Model;
using Report.Queuing.Model.QueueModel;

namespace Phone.Directory.Api
{
    public static class ReportHandler
    {
        #region Methods

        /// <summary>
        /// The ReportPrepare.
        /// </summary>
        /// <param name="reportQueue">The baseQueueModel<see cref="ReportQueueModel"/>.</param>
        public static void ReportPrepare(ReportQueueModel reportQueue)
        {
            using var scope = new FakeContext();
            try
            {
               scope.ServiceProvider.GetServices<IReportService>()
                        .FirstOrDefault()?.GetReportInfos(reportQueue.ReportId);

            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion
    }
}

