﻿using System;
namespace Phone.Directory.Common.Interfaces
{
    public interface IJsonWebToken
    {
        #region Properties

        /// <summary>
        /// Gets or sets the Expires.
        /// </summary>
        long Expires { get; set; }

        /// <summary>
        /// Gets or sets the Token.
        /// </summary>
        string Token { get; set; }

        #endregion
    }
}

