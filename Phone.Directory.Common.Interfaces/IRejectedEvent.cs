﻿using System;
namespace Phone.Directory.Common.Interfaces
{
    public interface IRejectedEvent : IEvent
    {
        #region Properties

        /// <summary>
        /// Gets the Code.
        /// </summary>
        string Code { get; }

        /// <summary>
        /// Gets the Reason.
        /// </summary>
        string Reason { get; }

        #endregion
    }
}

