﻿using System;
namespace Phone.Directory.Infrastructure.Attributes
{
    public sealed class CharEnumAttribute : Attribute
    {
        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="CharEnumAttribute"/> class.
        /// </summary>
        /// <param name="value">The value<see cref="char"/>.</param>
        public CharEnumAttribute(char value) => this.Value = value;

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the Value.
        /// </summary>
        public char Value { get; set; }

        #endregion
    }
}

