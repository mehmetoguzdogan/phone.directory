﻿using System;
namespace Phone.Directory.Infrastructure.Attributes
{
    public sealed class GuidEnumAttribute : Attribute
    {
        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="GuidEnumAttribute"/> class.
        /// </summary>
        /// <param name="s"> The s. </param>
        public GuidEnumAttribute(string s) => this.Guid = Guid.Parse(s);

        #endregion

        #region Properties

        /// <summary>
        /// Gets the Guid
        /// </summary>
        public Guid Guid { get; private set; }

        #endregion
    }
}

