﻿using System;
namespace Phone.Directory.Infrastructure.Attributes
{
    public sealed class StringEnumAttribute : Attribute
    {
        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="StringEnumAttribute"/> class.
        /// </summary>
        /// <param name="value">The value<see cref="string"/></param>
        public StringEnumAttribute(string value) => Value = value;

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the Value
        /// </summary>
        public string Value { get; set; }

        #endregion
    }
}

