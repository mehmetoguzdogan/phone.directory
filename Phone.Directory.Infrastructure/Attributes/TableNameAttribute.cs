﻿using System;
namespace Phone.Directory.Infrastructure.Attributes
{
    public sealed class TableNameAttribute : Attribute
    {
        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="TableNameAttribute"/> class.
        /// </summary>
        /// <param name="s"> The s. </param>
        public TableNameAttribute(string s) => this.TableName = s;

        #endregion

        #region Properties

        /// <summary>
        /// Gets the TableName
        /// </summary>
        public string TableName { get; private set; }

        #endregion
    }
}

