﻿using System;
using Phone.Directory.Infrastructure.Const;

namespace Phone.Directory.Infrastructure.Attributes
{
    public sealed class TableTransactionAttribute : Attribute
    {
        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="TableTransactionAttribute"/> class.
        /// </summary>
        /// <param name="transactionName">The transactionName<see cref="string"/></param>
        public TableTransactionAttribute(string transactionName = TableTransactions.SEMI_TRANSACTION) => TransactionName = transactionName;

        #endregion

        #region Properties

        /// <summary>
        /// Gets the TransactionName
        /// </summary>
        public string TransactionName { get; private set; }

        #endregion
    }
}

