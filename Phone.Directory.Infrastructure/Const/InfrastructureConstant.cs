﻿using System;
namespace Phone.Directory.Infrastructure.Const
{
    public static class InfrastructureConstant
    {
        #region Constants

        /// <summary>
        /// Defines the EMAIL_REGEX
        /// </summary>
        internal const string EMAIL_REGEX = @"^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$";

        /// <summary>
        /// Defines the IPV4_REGEX
        /// </summary>
        internal const string IPV4_REGEX = "^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])$";

        #endregion
    }
}

