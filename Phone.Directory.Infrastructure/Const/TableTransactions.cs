﻿using System;
namespace Phone.Directory.Infrastructure.Const
{
    public static class TableTransactions
    {
        /// <summary>
        /// Defines the TRANSACTION
        /// </summary>
        public const string TRANSACTION = "Tr";

        /// <summary>
        /// Defines the SEMI_TRANSACTION
        /// </summary>
        public const string SEMI_TRANSACTION = "St";

        /// <summary>
        /// Defines the META_TRANSACTION
        /// </summary>
        public const string META_TRANSACTION = "Mt";
    }
}

