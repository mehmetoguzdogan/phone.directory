﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using Phone.Directory.Infrastructure.Repository;
using Phone.Directory.Infrastructure.Extensions;

namespace Phone.Directory.Infrastructure.DbContext
{
    public class PostgreDbContext : Microsoft.EntityFrameworkCore.DbContext
    {
        #region Fields

        /// <summary>
        /// Defines the connection
        /// </summary>
        protected readonly string connection;

        /// <summary>
        /// Defines the defaultSchema
        /// </summary>
        protected readonly string defaultSchema;

        /// <summary>
        /// Defines the loggerFactory
        /// </summary>
        protected readonly ILoggerFactory loggerFactory;

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="PostgreDbContext"/> class.
        /// </summary>
        /// <param name="connection">The connection<see cref="string"/></param>
        /// <param name="loggerFactory">The loggerFactory<see cref="ILoggerFactory"/></param>
        /// <param name="defaultSchema">The defaultSchema<see cref="string"/></param>
        public PostgreDbContext(string connection, ILoggerFactory loggerFactory, string defaultSchema)
        {
            this.loggerFactory = loggerFactory;
            this.connection = connection;
            this.defaultSchema = defaultSchema;
            Database.SetCommandTimeout(300);
        }

        #endregion

        #region Methods

        /// <summary>
        /// The AddRange
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="entities">The entities<see cref="IEnumerable{T}"/></param>
        public void AddRange<T>(IEnumerable<T> entities) where T : class
            => base.AddRange(entities);


        /// <summary>
        /// The Base
        /// </summary>
        /// <returns>The <see cref="DbContext"/></returns>
        public Microsoft.EntityFrameworkCore.DbContext Base() => this;


        /// <summary>
        /// The ChangeState
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="entity">The entity<see cref="T"/></param>
        /// <param name="state">The state<see cref="DomainState"/></param>
        public void ChangeState<T>(T entity, DomainState state) where T : class
        {
            ////throw new NotImplementedException();
        }

        /// <summary>
        /// The EntrySetValues
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="item">The item<see cref="T"/></param>
        /// <param name="entity">The entity<see cref="T"/></param>
        public void EntrySetValues<T>(T item, T entity) where T : class
        {
            ////throw new NotImplementedException();
        }

        /// <summary>
        /// The ExecuteRawSqlCommand
        /// </summary>
        /// <param name="sql">The sql<see cref="string"/></param>
        public void ExecuteRawSqlCommand(string sql) => this.Database.ExecuteSqlRaw(sql);

        /// <summary>
        /// The ExecuteSqlCommand
        /// </summary>
        /// <param name="sql">The sql<see cref="string"/></param>
        /// <param name="timeout">The timeout<see cref="int?"/></param>
        /// <param name="parameters">The parameters<see cref="object []"/></param>
        /// <returns>The <see cref="int"/></returns>
        public int ExecuteSqlCommand(string sql, int? timeout = null, params object[] parameters)
        {
            int? previousTimeout = null;
            if (timeout.HasValue)
            {
                this.Database.SetCommandTimeout(TimeSpan.FromSeconds((double)timeout));
                previousTimeout = this.Database.GetCommandTimeout();
            }

            int result = this.Database.ExecuteSqlRaw(sql, parameters);
            if (previousTimeout.HasValue)
            {
                this.Database.SetCommandTimeout(TimeSpan.FromSeconds((double)previousTimeout));
            }

            return result;
        }

        /// <summary>
        /// The ExecuteStoredProcedure
        /// </summary>
        /// <param name="procedureName">The procedureName<see cref="string"/></param>
        /// <param name="sqlParameters">The sqlParameters<see cref="object []"/></param>
        /// <returns>The <see cref="int"/></returns>
        public int ExecuteStoredProcedure(string procedureName, params object[] parameters)
            => this.Database.ExecuteSqlRaw(procedureName, parameters);

        /// <summary>
        /// The ExecuteStoredProcedure
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="procedureName">The procedureName<see cref="string"/></param>
        /// <param name="sqlParameters">The sqlParameters<see cref="object []"/></param>
        /// <returns>The <see cref="IEnumerable{T}"/></returns>
        public IEnumerable<T> ExecuteStoredProcedure<T>(string procedureName, params object[] sqlParameters)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// The OnConfiguring
        /// </summary>
        /// <param name="optionsBuilder">The optionsBuilder<see cref="DbContextOptionsBuilder"/></param>
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder
                .UseLazyLoadingProxies()
                .UseNpgsql(connectionString: connection);

#if DEBUG
            optionsBuilder.UseLoggerFactory(loggerFactory);
#endif
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder = modelBuilder.HasDefaultSchema(defaultSchema);
            base.OnModelCreating(modelBuilder);

            foreach (var entity in modelBuilder.Model.GetEntityTypes())
            {
                // Replace table names
                entity.SetTableName($"{entity.GetTableName().ToSnakeCase()}");

                // Replace column names            
                foreach (var property in entity.GetProperties())
                {
                    property.SetColumnName(property.Name.ToSnakeCase());
                }

                foreach (var key in entity.GetKeys())
                {
                    key.SetName(key.GetName().ToSnakeCase());
                }

                foreach (var key in entity.GetForeignKeys())
                {
                    key.SetConstraintName(key.GetConstraintName().ToSnakeCase());
                }

                foreach (var index in entity.GetIndexes())
                {
                    index.SetName(index.GetDatabaseName().ToSnakeCase());
                }
            }
        }

        #endregion
    }
}

