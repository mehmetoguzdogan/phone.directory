﻿using System;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Configuration;

namespace Phone.Directory.Infrastructure.Engine
{
    public interface IConfigurationRegister
    {
        #region Properties

        /// <summary>
        /// Gets the Order
        /// </summary>
        int Order { get; }

        #endregion

        #region Methods

        /// <summary>
        /// The Configure
        /// </summary>
        /// <param name="service">The service<see cref="IServiceCollection"/></param>
        /// <param name="configuration">The configuration<see cref="IConfiguration"/></param>
        void Configure(IServiceCollection service, IConfiguration configuration);

        #endregion
    }
}

