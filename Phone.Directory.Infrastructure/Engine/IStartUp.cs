﻿
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace Phone.Directory.Infrastructure.Engine
{
    public interface IStartUp
    {
        #region Properties

        /// <summary>
        /// Gets the Configuration
        /// </summary>
        IConfiguration Configuration { get; }

        #endregion

        #region Methods

        /// <summary>
        /// The Configure Services
        /// </summary>
        /// <param name="services">The services<see cref="IServiceCollection"/></param>
        void ConfigureServices(IServiceCollection services);

        /// <summary>
        /// The Configure
        /// </summary>
        /// <param name="app">The app<see cref="IApplicationBuilder"/></param>
        /// <param name="env">The env<see cref="IHostApplicationLifetime"/></param>
        void Configure(IApplicationBuilder app, IHostApplicationLifetime env);

        #endregion
    }
}

