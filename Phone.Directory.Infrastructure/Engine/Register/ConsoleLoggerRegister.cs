﻿using System;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Phone.Directory.Infrastructure.Logs;

namespace Phone.Directory.Infrastructure.Engine.Register
{
    public static class ConsoleLoggerRegister
    {
        #region Methods

        /// <summary>
        /// The RegisterSetting
        /// </summary>
        /// <param name="service">The service<see cref="IServiceCollection"/></param>
        public static void RegisterConsoleLogger(this IServiceCollection service)
        {
            // write ef query to output window.
            var logger = new LoggerFactory();
            service.AddSingleton(new ConsoleLoggerFactory(logger));
        }

        #endregion
    }
}

