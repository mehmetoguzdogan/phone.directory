﻿
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Linq;
using Phone.Directory.Infrastructure.Engine.Register;
using Phone.Directory.Infrastructure.Configuration;
using Phone.Directory.Infrastructure.Extensions;
using Phone.Directory.Infrastructure.Helpers;

namespace Phone.Directory.Infrastructure.Engine
{
    public abstract class StartupBase
    {
        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="StartupBase"/> class.
        /// </summary>
        /// <param name="configuration">The configuration<see cref="IConfiguration"/></param>
        protected StartupBase(IConfiguration configuration) => Configuration = configuration;

        #endregion

        #region Properties

        /// <summary>
        /// Gets the Configuration
        /// </summary>
        public IConfiguration Configuration { get; }

        #endregion

        #region Methods

        /// <summary>
        /// The Configure
        /// </summary>
        /// <param name="app">The app<see cref="IApplicationBuilder"/></param>
        /// <param name="env">The env<see cref="IHostingEnvironment"/></param>
        /// <param name="loggerFactory">The loggerFactory<see cref="ILoggerFactory"/></param>
        public void Configure(IApplicationBuilder app, IHostApplicationLifetime env)
        {
            CommonHelper.GetTypesImplementingInterface<IDynamicConfiguration>()?
                .ForEach(x => ((IDynamicConfiguration)Activator.CreateInstance(x)).Configure(app, env));

            CustomConfigure(app, env);
        }

        /// <summary>
        /// The ConfigureServices
        /// </summary>
        /// <param name="services">The services<see cref="IServiceCollection"/></param>
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddHttpContextAccessor();
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            services.RegisterConsoleLogger();

            CommonHelper.GetAllInstancesOf<IDynamicRegister>()?
                .ForEach(x => x.Configure(services));

            CustomConfigureServices(services);
        }

        /// <summary>
        /// The CustomConfigure
        /// </summary>
        /// <param name="app">The app<see cref="IApplicationBuilder"/></param>
        /// <param name="env">The env<see cref="IHostingEnvironment"/></param>
        /// <param name="loggerFactory">The loggerFactory<see cref="ILoggerFactory"/></param>
        public abstract void CustomConfigure(IApplicationBuilder app, IHostApplicationLifetime env);

        /// <summary>
        /// The ConfigureService
        /// </summary>
        /// <param name="services">The services<see cref="IServiceCollection"/></param>
        public abstract void CustomConfigureServices(IServiceCollection services);

        #endregion
    }
}

