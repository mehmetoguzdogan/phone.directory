﻿using System;
namespace Phone.Directory.Infrastructure.Entity
{
    public interface IEntity<TId, TUserId> : IBaseEntity<TId>
    {
        #region Properties

        /// <summary>
        /// Gets or sets the CreatedBy
        /// </summary>
        TUserId CreatedBy { get; set; }

        /// <summary>
        /// Gets or sets the CreatedDate
        /// </summary>
        DateTime CreatedDate { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether IsActive
        /// </summary>
        bool IsActive { get; set; }

        /// <summary>
        /// Gets or sets the UpdatedBy
        /// </summary>
        TUserId UpdatedBy { get; set; }

        /// <summary>
        /// Gets or sets the UpdatedDate
        /// </summary>
        DateTime? UpdatedDate { get; set; }

        #endregion
    }
}

