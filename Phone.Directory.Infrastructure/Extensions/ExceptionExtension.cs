﻿using System;
using System.Text;

namespace Phone.Directory.Infrastructure.Extensions
{
    public static class ExceptionExtension
    {
        #region Methods

        /// <summary>
        /// The GetExceptionMessage
        /// </summary>
        /// <param name="exception">The exception<see cref="Exception"/></param>
        /// <returns>The <see cref="string"/></returns>
        public static string GetExceptionMessage(this Exception exception)
        {
            if (exception == default(Exception))
            {
                return string.Empty;
            }

            StringBuilder stringBuilder = new StringBuilder();

            while (exception != null)
            {
                stringBuilder.AppendLine(exception.Message);
                stringBuilder.AppendLine(Environment.NewLine);

                if (!string.IsNullOrEmpty(exception.StackTrace))
                {
                    stringBuilder.AppendLine(exception.StackTrace);
                    stringBuilder.AppendLine(Environment.NewLine);
                }

                exception = exception.InnerException;
            }

            return stringBuilder.ToString();
        }

        #endregion
    }
}

