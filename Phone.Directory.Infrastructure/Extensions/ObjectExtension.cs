﻿using System;
using System.Globalization;
using System.Runtime.Serialization.Formatters.Binary;
using Phone.Directory.Infrastructure.Helpers;

namespace Phone.Directory.Infrastructure.Extensions
{
    public static class ObjectExtension
    {
        #region Methods

        /// <summary>
        /// The is data changed.
        /// </summary>
        /// <typeparam name="T"> Type of values to compare. </typeparam>
        /// <param name="x"> The x. </param>
        /// <param name="y"> The y. </param>
        /// <param name="propertyName"> The property name. </param>
        /// <param name="forceOverwrite"> The is Overwritable. </param>
        /// <returns> The <see cref="bool"/>. </returns>
        public static bool IsDataChanged<T>(this object x, object y, string propertyName, bool forceOverwrite = false)
        {
            var xPropInfo = x.GetType().GetProperty(propertyName);
            var yPropInfo = y.GetType().GetProperty(propertyName);

            var xValue = (T)xPropInfo?.GetValue(x, null);
            var yValue = (T)yPropInfo?.GetValue(y, null);

            if (EqualityComparer<T>.Default.Equals(xValue, yValue))
            {
                return false;
            }

            if (forceOverwrite)
            {
                xPropInfo?.SetValue(x, yValue);
            }

            return true;
        }

        /// <summary>
        /// The is direct data changed.
        /// </summary>
        /// <typeparam name="T"> Type of parameter. </typeparam>
        /// <param name="x"> The x. </param>
        /// <param name="y"> The y. </param>
        /// <param name="propertyName"> The property name. </param>
        /// <param name="forceOverwrite"> The force overwrite. </param>
        /// <returns> The <see cref="bool"/>. </returns>
        public static bool IsDirectDataChanged<T>(this object x, object y, string propertyName, bool forceOverwrite = false)
        {
            var xPropInfo = x.GetType().GetProperty(propertyName);

            var xValue = (T)xPropInfo?.GetValue(x, null);
            var yValue = (T)y;

            if (EqualityComparer<T>.Default.Equals(xValue, yValue))
            {
                return false;
            }

            if (forceOverwrite)
            {
                xPropInfo?.SetValue(x, yValue);
            }

            return true;
        }

        /// <summary>
        /// The to.
        /// </summary>
        /// <typeparam name="T"> Type of object. </typeparam>
        /// <param name="value"> The value. </param>
        /// <returns> The <see cref="T"/>. </returns>
        public static T To<T>(this object value) => (T)To(value, typeof(T));

        /// <summary>
        /// The to.
        /// </summary>
        /// <typeparam name="T"> Generic Type. </typeparam>
        /// <param name="value"> The value. </param>
        /// <param name="defaultValue"> The default value. </param>
        /// <returns> The <see cref="T"/> The T </returns>
        public static T To<T>(this object value, object defaultValue)
        {
            try
            {
                return (T)To(value, typeof(T));
            }
            catch
            {
                return (T)To(defaultValue, typeof(T));
            }
        }

        /// <summary>
        /// The to.
        /// </summary>
        /// <param name="value"> The value. </param>
        /// <param name="destinationType"> The destination type. </param>
        /// <returns> The <see cref="object"/>. </returns>
        public static object To(this object value, Type destinationType) => To(value, destinationType, CultureInfo.InvariantCulture);

        /// <summary>
        /// The to.
        /// </summary>
        /// <param name="value"> The value. </param>
        /// <param name="destinationType"> The destination type. </param>
        /// <param name="culture"> The culture. </param>
        /// <returns> The <see cref="object"/>. </returns>
        public static object To(this object value, Type destinationType, CultureInfo culture)
        {
            if (value == null)
            {
                return null;
            }

            var sourceType = value.GetType();

            var destinationConverter = CommonHelper.GetCustomTypeConverter(destinationType);
            var sourceConverter = CommonHelper.GetCustomTypeConverter(sourceType);

            if (destinationConverter != null && destinationConverter.CanConvertFrom(value.GetType()))
            {
                return destinationConverter.ConvertFrom(null, culture, value);
            }

            if (sourceConverter != null && sourceConverter.CanConvertTo(destinationType))
            {
                return sourceConverter.ConvertTo(null, culture, value, destinationType);
            }

            if (destinationType.IsEnum && value is int)
            {
                return System.Enum.ToObject(destinationType, (int)value);
            }

            if (!destinationType.IsInstanceOfType(value))
            {
                return Convert.ChangeType(value, destinationType, culture);
            }

            return value;
        }

        /// <summary>
        /// The object to byte array.
        /// </summary>
        /// <param name="obj"> The obj. </param>
        /// <returns> The <see cref="Byte"/>. </returns>
        public static byte[] ToByteArray(this object obj)
        {
            if (obj == null)
            {
                return default;
            }

            var bf = new BinaryFormatter();
            using var ms = new MemoryStream();
            bf.Serialize(ms, obj);

            return ms.ToArray();
        }

        /// <summary>
        /// The byte array to object.
        /// </summary>
        /// <typeparam name="T"> Type of object. </typeparam>
        /// <param name="arrBytes"> The arr bytes. </param>
        /// <returns> The <see cref="T"/>. </returns>
        public static T ToObject<T>(this byte[] arrBytes)
        {
            if (arrBytes == null)
            {
                return default;
            }

            var memStream = new MemoryStream();
            var binForm = new BinaryFormatter();
            memStream.Write(arrBytes, 0, arrBytes.Length);
            memStream.Seek(0, SeekOrigin.Begin);
            var obj = binForm.Deserialize(memStream);

            return (T)obj;
        }

        #endregion
    }
}

