﻿using System;
using System.ComponentModel;
using System.Text.RegularExpressions;

namespace Phone.Directory.Infrastructure.Extensions
{
    public static class StringExtension
    {
        #region Methods

        /// <summary>
        /// The Parse.
        /// </summary>
        /// <typeparam name="T">.</typeparam>
        /// <param name="value">The value<see cref="string"/>.</param>
        /// <returns>The <see cref="T"/>.</returns>
        public static T Parse<T>(this string value)
        {
            T result = default(T);
            if (!string.IsNullOrEmpty(value))
            {
                TypeConverter tc = TypeDescriptor.GetConverter(typeof(T));
                result = (T)tc.ConvertFrom(value);
            }
            return result;
        }

        /// <summary>
        /// The ParseChar.
        /// </summary>
        /// <param name="value">The value<see cref="string"/>.</param>
        /// <returns>The <see cref="char"/>.</returns>
        public static char ParseChar(this string value)
        {
            return value.Parse<char>();
        }

        /// <summary>
        /// The ToSnakeCase.
        /// </summary>
        /// <param name="input">The input<see cref="string"/>.</param>
        /// <returns>The <see cref="string"/>.</returns>
        public static string ToSnakeCase(this string input)
        {
            if (string.IsNullOrEmpty(input))
            {
                return input;
            }

            var startUnderscores = Regex.Match(input, @"^_+");
            ////return startUnderscores + Regex.Replace(input, @"([a-z0-9])([A-Z])", "$1_$2").ToLowerInvariant();
            return $"{startUnderscores}{input.ToLowerInvariant()}";
        }

        #endregion
    }
}

