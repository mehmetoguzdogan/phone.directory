﻿using System;
using System.Xml;
using System.Xml.Serialization;

namespace Phone.Directory.Infrastructure.Extensions
{
    public static class XmlExtension
    {
        #region Methods

        /// <summary>
        /// The Deserialize
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="input">The input<see cref="string"/></param>
        /// <returns>The <see cref="T"/></returns>
        public static T Deserialize<T>(string input) where T : class
        {
            var ser = new XmlSerializer(typeof(T));
            using (var sr = new StringReader(input))
            {
                return (T)ser.Deserialize(sr);
            }
        }

        /// <summary>
        /// The Serialize
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="dataToSerialize">The dataToSerialize<see cref="T"/></param>
        /// <param name="types">The types<see cref="Type[]"/></param>
        /// <returns>The <see cref="string"/></returns>
        public static string Serialize<T>(T dataToSerialize, Type[] types = null)
        {
            var stringWriter = new System.IO.StringWriter();
            var ns = new XmlSerializerNamespaces();
            ns.Add("", "");

            using (var writer = XmlWriter.Create(stringWriter, new XmlWriterSettings { Indent = false, OmitXmlDeclaration = true }))
            {
                if (types != null)
                {
                    new XmlSerializer(typeof(T), types).Serialize(writer, dataToSerialize, ns);
                    return stringWriter.ToString();
                }

                new XmlSerializer(typeof(T)).Serialize(writer, dataToSerialize, ns);
                return stringWriter.ToString();
            }
        }

        #endregion
    }
}

