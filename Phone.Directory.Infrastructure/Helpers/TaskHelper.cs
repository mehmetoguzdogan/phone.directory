﻿using System;
namespace Phone.Directory.Infrastructure.Helpers
{
    public static class TaskHelper
    {
        #region Methods

        /// <summary>
        /// The RunActionAsync
        /// </summary>
        /// <param name="action">The action<see cref="Action"/></param>
        /// <returns>The <see cref="Task"/></returns>
        public static async Task RunActionAsync(Action action) => await Task.Run(() => action());

        /// <summary>
        /// The RunActionInTask
        /// </summary>
        /// <param name="action">The action<see cref="Action"/></param>
        /// <returns>The <see cref="Task"/></returns>
        public static Task RunActionInTask(Action action) => Task.Run(() => action());

        /// <summary>
        /// The RunFunctionAsync
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="function">The function<see cref="Func{T}"/></param>
        /// <returns>The <see cref="Task{T}"/></returns>
        public static async Task<T> RunFunctionAsync<T>(Func<T> function) => await Task.Run(() => function());

        #endregion
    }
}

