﻿using System;
using System.Transactions;

namespace Phone.Directory.Infrastructure.Helpers
{
    public static class TransactionHelper
    {
        #region Methods

        /// <summary>
        /// The create nolock transaction.
        /// </summary>
        /// <returns> The <see cref="TransactionScope"/>. </returns>
        public static TransactionScope CreateNoLockTransaction()
        {
            var options = new TransactionOptions { IsolationLevel = IsolationLevel.ReadUncommitted };

            return new TransactionScope(TransactionScopeOption.Required, options, TransactionScopeAsyncFlowOption.Enabled);
        }

        #endregion
    }
}

