﻿using System;
namespace Phone.Directory.Infrastructure.Helpers
{
    public static class TransientFaultHandler
    {
        #region Methods

        /// <summary>
        /// The do.
        /// </summary>
        /// <param name="action"> The action. </param>
        /// <param name="retryInterval"> The retry interval. </param>
        /// <param name="retryCount"> The retry count. </param>
        public static void Do(
            Action action,
            TimeSpan retryInterval,
            int retryCount = 3)
            => Do<object>(
                () =>
                {
                    action();
                    return null;
                },
                retryInterval,
                retryCount);

        /// <summary>
        /// The do.
        /// </summary>
        /// <typeparam name="T"> Type of return value. </typeparam>
        /// <param name="action"> The action. </param>
        /// <param name="retryInterval"> The retry interval. </param>
        /// <param name="retryCount"> The retry count. </param>
        /// <returns> The <see cref="T"/>. </returns>
        public static T Do<T>(
            Func<T> action,
            TimeSpan retryInterval,
            int retryCount = 3)
        {
            var exceptions = new List<Exception>();

            for (var retry = 0; retry < retryCount; retry++)
            {
                try
                {
                    return action();
                }
                catch (Exception ex)
                {
                    exceptions.Add(ex);
                    Thread.Sleep(retryInterval);
                }
            }

            throw new AggregateException(exceptions);
        }

        #endregion
    }
}

