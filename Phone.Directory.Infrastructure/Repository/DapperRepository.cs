﻿
using Dapper;
using Microsoft.Data.SqlClient;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;


namespace Phone.Directory.Infrastructure.Repository
{
    public class DapperRepository : IDapperRepository
    {
        #region Fields

        /// <summary>
        /// Defines the connectionString
        /// </summary>
        private readonly string connectionString;

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="DapperRepository"/> class.
        /// </summary>
        /// <param name="connectionString">The connectionString<see cref="string"/></param>
        public DapperRepository(string connectionString)
            => this.connectionString = connectionString;

        #endregion

        #region Methods

        /// <summary>
        /// The Execute
        /// </summary>
        /// <param name="query">The query<see cref="string"/></param>
        /// <param name="param">The param<see cref="object"/></param>
        /// <param name="commandType">The commandType<see cref="CommandType?"/></param>
        /// <param name="commandTimeout">The commandTimeout<see cref="int?"/></param>
        /// <returns>The <see cref="int"/></returns>
        public int Execute(string query, object param, CommandType? commandType, int? commandTimeout)
        {
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                return connection.Execute(query, param, commandType: commandType, commandTimeout: commandTimeout);
            }
        }

        /// <summary>
        /// The ExecuteAsync
        /// </summary>
        /// <param name="query">The query<see cref="string"/></param>
        /// <param name="param">The param<see cref="object"/></param>
        /// <param name="commandType">The commandType<see cref="CommandType?"/></param>
        /// <param name="commandTimeout">The commandTimeout<see cref="int?"/></param>
        /// <returns>The <see cref="Task{int}"/></returns>
        public async Task<int> ExecuteAsync(string query, object param, CommandType? commandType, int? commandTimeout)
        {
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                return await connection.ExecuteAsync(query, param, commandType: commandType, commandTimeout: commandTimeout);
            }
        }

        /// <summary>
        /// The Query
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="query">The query<see cref="string"/></param>
        /// <param name="param">The param<see cref="object"/></param>
        /// <param name="commandType">The commandType<see cref="CommandType"/></param>
        /// <param name="commandTimeout">The commandTimeout<see cref="int?"/></param>
        /// <returns>The <see cref="T"/></returns>
        public IEnumerable<T> Query<T>(string query, object param = null, CommandType? commandType = null, int? commandTimeout = null)
        {
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                return connection.Query<T>(query, param, commandType: commandType, commandTimeout: commandTimeout);
            }
        }

        /// <summary>
        /// The QueryAsync
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="query">The query<see cref="string"/></param>
        /// <param name="param">The param<see cref="object"/></param>
        /// <param name="commandType">The commandType<see cref="CommandType"/></param>
        /// <param name="commandTimeout">The commandTimeout<see cref="int?"/></param>
        /// <returns>The <see cref="Task{IEnumerable{T}}"/></returns>
        public async Task<IEnumerable<T>> QueryAsync<T>(string query, object param = null, CommandType? commandType = null, int? commandTimeout = null)
        {
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                return await connection.QueryAsync<T>(query, param, commandType: commandType, commandTimeout: commandTimeout);
            }
        }

        /// <summary>
        /// The QueryFirst
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="query">The query<see cref="string"/></param>
        /// <param name="param">The param<see cref="object"/></param>
        /// <param name="commandType">The commandType<see cref="CommandType?"/></param>
        /// <param name="commandTimeout">The commandTimeout<see cref="int?"/></param>
        /// <returns>The <see cref="T"/></returns>
        public T QueryFirst<T>(string query, object param = null, CommandType? commandType = null, int? commandTimeout = null)
        {
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                return connection.QueryFirst<T>(query, param, commandType: commandType, commandTimeout: commandTimeout);
            }
        }

        /// <summary>
        /// The QueryFirstAsync
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="query">The query<see cref="string"/></param>
        /// <param name="param">The param<see cref="object"/></param>
        /// <param name="commandType">The commandType<see cref="CommandType?"/></param>
        /// <param name="commandTimeout">The commandTimeout<see cref="int?"/></param>
        /// <returns>The <see cref="Task{T}"/></returns>
        public async Task<T> QueryFirstAsync<T>(string query, object param = null, CommandType? commandType = null, int? commandTimeout = null)
        {
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                return await connection.QueryFirstAsync<T>(query, param, commandType: commandType, commandTimeout: commandTimeout);
            }
        }

        /// <summary>
        /// The QueryFirstOrDefault
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="query">The query<see cref="string"/></param>
        /// <param name="param">The param<see cref="object"/></param>
        /// <param name="commandType">The commandType<see cref="CommandType?"/></param>
        /// <param name="commandTimeout">The commandTimeout<see cref="int?"/></param>
        /// <returns>The <see cref="T"/></returns>
        public T QueryFirstOrDefault<T>(string query, object param = null, CommandType? commandType = null, int? commandTimeout = null)
        {
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                return connection.QueryFirstOrDefault<T>(query, param, commandType: commandType, commandTimeout: commandTimeout);
            }
        }

        /// <summary>
        /// The QueryFirstOrDefaultAsync
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="query">The query<see cref="string"/></param>
        /// <param name="param">The param<see cref="object"/></param>
        /// <param name="commandType">The commandType<see cref="CommandType?"/></param>
        /// <param name="commandTimeout">The commandTimeout<see cref="int?"/></param>
        /// <returns>The <see cref="Task{T}"/></returns>
        public async Task<T> QueryFirstOrDefaultAsync<T>(string query, object param = null, CommandType? commandType = null, int? commandTimeout = null)
        {
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                return await connection.QueryFirstOrDefaultAsync<T>(query, param, commandType: commandType, commandTimeout: commandTimeout);
            }
        }

        /// <summary>
        /// The QuerySingle
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="query">The query<see cref="string"/></param>
        /// <param name="param">The param<see cref="object"/></param>
        /// <param name="commandType">The commandType<see cref="CommandType?"/></param>
        /// <param name="commandTimeout">The commandTimeout<see cref="int?"/></param>
        /// <returns>The <see cref="T"/></returns>
        public T QuerySingle<T>(string query, object param = null, CommandType? commandType = null, int? commandTimeout = null)
        {
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                return connection.QuerySingle<T>(query, param, commandType: commandType, commandTimeout: commandTimeout);
            }
        }

        /// <summary>
        /// The QuerySingleAsync
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="query">The query<see cref="string"/></param>
        /// <param name="param">The param<see cref="object"/></param>
        /// <param name="commandType">The commandType<see cref="CommandType?"/></param>
        /// <param name="commandTimeout">The commandTimeout<see cref="int?"/></param>
        /// <returns>The <see cref="Task{T}"/></returns>
        public async Task<T> QuerySingleAsync<T>(string query, object param = null, CommandType? commandType = null, int? commandTimeout = null)
        {
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                return await connection.QuerySingleAsync<T>(query, param, commandType: commandType, commandTimeout: commandTimeout);
            }
        }

        /// <summary>
        /// The QuerySingleOrDefault
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="query">The query<see cref="string"/></param>
        /// <param name="param">The param<see cref="object"/></param>
        /// <param name="commandType">The commandType<see cref="CommandType?"/></param>
        /// <param name="commandTimeout">The commandTimeout<see cref="int?"/></param>
        /// <returns>The <see cref="T"/></returns>
        public T QuerySingleOrDefault<T>(string query, object param = null, CommandType? commandType = null, int? commandTimeout = null)
        {
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                return connection.QuerySingleOrDefault<T>(query, param, commandType: commandType, commandTimeout: commandTimeout);
            }
        }

        /// <summary>
        /// The QuerySingleOrDefaultAsync
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="query">The query<see cref="string"/></param>
        /// <param name="param">The param<see cref="object"/></param>
        /// <param name="commandType">The commandType<see cref="CommandType?"/></param>
        /// <param name="commandTimeout">The commandTimeout<see cref="int?"/></param>
        /// <returns>The <see cref="Task{T}"/></returns>
        public async Task<T> QuerySingleOrDefaultAsync<T>(string query, object param = null, CommandType? commandType = null, int? commandTimeout = null)
        {
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                return await connection.QuerySingleOrDefaultAsync<T>(query, param, commandType: commandType, commandTimeout: commandTimeout);
            }
        }

        #endregion
    }
}

