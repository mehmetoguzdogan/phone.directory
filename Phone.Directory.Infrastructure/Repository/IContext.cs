﻿using System;
using Microsoft.EntityFrameworkCore;

namespace Phone.Directory.Infrastructure.Repository
{
    public interface IContext
    {
        #region Methods

        /// <summary>
        /// The add range.
        /// </summary>
        /// <typeparam name="T"> Type of entity. </typeparam>
        /// <param name="entities">The entities<see cref="IEnumerable{T}"/></param>
        void AddRange<T>(IEnumerable<T> entities) where T : class;

        /// <summary>
        /// The base.
        /// </summary>
        /// <returns>The <see cref="DbContext"/></returns>
        Microsoft.EntityFrameworkCore.DbContext Base();

        /// <summary>
        /// The ChangeState
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="entity">The entity<see cref="T"/></param>
        /// <param name="state">The state<see cref="DomainState"/></param>
        void ChangeState<T>(T entity, DomainState state) where T : class;

        /// <summary>
        /// The EntrySetValues
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="item">The item<see cref="T"/></param>
        /// <param name="entity">The entity<see cref="T"/></param>
        void EntrySetValues<T>(T item, T entity) where T : class;

        /// <summary>
        /// The execute raw sql command.
        /// </summary>
        /// <param name="sql">The sql<see cref="string"/></param>
        void ExecuteRawSqlCommand(string sql);

        /// <summary>
        /// The execute sql command.
        /// </summary>
        /// <param name="sql"> The sql. </param>
        /// <param name="timeout"> The timeout. </param>
        /// <param name="parameters"> The parameters. </param>
        /// <returns> The <see cref="int"/>. </returns>
        int ExecuteSqlCommand(string sql, int? timeout = null, params object[] parameters);

        /// <summary>
        /// The execute stored procedure.
        /// </summary>
        /// <param name="procedureName"> The procedure name. </param>
        /// <param name="sqlParameters"> The sql parameters. </param>
        /// <returns> The <see cref="int"/>. </returns>
        int ExecuteStoredProcedure(string procedureName, params object[] sqlParameters);

        /// <summary>
        /// The execute stored procedure.
        /// </summary>
        /// <typeparam name="T"> Type of domainmodel. </typeparam>
        /// <param name="procedureName"> The procedure name. </param>
        /// <param name="sqlParameters"> The sql parameters. </param>
        /// <returns> The <see cref="IEnumerable{T}"/>. </returns>
        IEnumerable<T> ExecuteStoredProcedure<T>(string procedureName, params object[] sqlParameters);

        /// <summary>
        /// The save changes.
        /// </summary>
        /// <returns> The <see cref="int"/>. </returns>
        int SaveChanges();

        /// <summary>
        /// The SaveChangesAsync
        /// </summary>
        /// <param name="cancellationToken">The cancellationToken<see cref="CancellationToken"/></param>
        /// <returns>The <see cref="Task{int}"/></returns>
        Task<int> SaveChangesAsync(CancellationToken cancellationToken = default(CancellationToken));

        /// <summary>
        /// The set.
        /// </summary>
        /// <typeparam name="T"> Type of entity. </typeparam>
        /// <returns> The <see cref="DbSet{T}"/>. </returns>
        DbSet<T> Set<T>() where T : class;

        #endregion
    }
}

