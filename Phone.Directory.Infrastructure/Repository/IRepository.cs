﻿using System;
using Phone.Directory.Infrastructure.Entity;

namespace Phone.Directory.Infrastructure.Repository
{
    public interface IRepository<T, TId> : IReadOnlyRepository<T, TId>, IWriteOnlyRepository<T, TId> where T : IBaseEntity<TId>
    {
    }
}

