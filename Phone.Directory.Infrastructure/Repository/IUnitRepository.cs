﻿using System;
using Phone.Directory.Infrastructure.Entity;

namespace Phone.Directory.Infrastructure.Repository
{
    public interface IUnitRepository<T, TId> : IReadOnlyRepository<T, TId>, IWriteOnlyUnitRepository<T, TId> where T : IBaseEntity<TId>
    {
    }
}

