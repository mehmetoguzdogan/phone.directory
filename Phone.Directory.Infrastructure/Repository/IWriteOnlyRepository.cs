﻿using System;
using Phone.Directory.Infrastructure.Entity;

namespace Phone.Directory.Infrastructure.Repository
{
    public interface IWriteOnlyRepository<T, TId> where T : IBaseEntity<TId>
    {
        #region Methods

        /// <summary>
        /// The Add
        /// </summary>
        /// <param name="entities">The entities<see cref="ICollection{T}"/></param>
        /// <returns>The <see cref="ICollection{T}"/></returns>
        ICollection<T> Add(ICollection<T> entities);

        /// <summary>
        /// The add.
        /// </summary>
        /// <param name="entity"> The entity. </param>
        /// <returns> The <see cref="TId"/>. </returns>
        TId Add(T entity);

        /// <summary>
        /// The AddAsync
        /// </summary>
        /// <param name="entities">The entities<see cref="IEnumerable{T}"/></param>
        /// <returns>The <see cref="Task{ICollection{T}}"/></returns>
        Task<ICollection<T>> AddAsync(ICollection<T> entities);

        /// <summary>
        /// The add asyn.
        /// </summary>
        /// <param name="entity"> The entity. </param>
        /// <returns> The <see cref="TId"/>. </returns>
        Task<TId> AddAsync(T entity);

        /// <summary>
        /// The Delete
        /// </summary>
        /// <param name="entities">The entities<see cref="ICollection{T}"/></param>
        void Delete(ICollection<T> entities);

        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="entity"> The entity. </param>
        /// <returns> The <see cref="int"/>. </returns>
        int Delete(T entity);

        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="key"> The key. </param>
        /// <returns> The <see cref="bool"/>. </returns>
        bool Delete(TId key);

        /// <summary>
        /// The DeleteAsync
        /// </summary>
        /// <param name="entities">The entities<see cref="ICollection{T}"/></param>
        /// <returns>The <see cref="Task"/></returns>
        Task DeleteAsync(ICollection<T> entities);

        /// <summary>
        /// The delete async.
        /// </summary>
        /// <param name="entity"> The entity. </param>
        /// <returns> The <see cref="Task"/>. </returns>
        Task<int> DeleteAsync(T entity);

        /// <summary>
        /// The delete async.
        /// </summary>
        /// <param name="key"> The key. </param>
        /// <returns> The <see cref="Task"/>. </returns>
        Task<bool> DeleteAsync(TId key);

        /// <summary>
        /// The Update
        /// </summary>
        /// <param name="entities">The entities<see cref="IEnumerable{T}"/></param>
        /// <returns>The <see cref="ICollection{T}"/></returns>
        ICollection<T> Update(ICollection<T> entities);

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="key"> The key. </param>
        /// <param name="entity"> The entity. </param>
        /// <returns> The <see cref="T"/>. </returns>
        T Update(TId key, T entity);

        /// <summary>
        /// The UpdateAsync
        /// </summary>
        /// <param name="entities">The entities<see cref="ICollection{T}"/></param>
        /// <returns>The <see cref="Task{ICollection{T}}"/></returns>
        Task<ICollection<T>> UpdateAsync(ICollection<T> entities);

        /// <summary>
        /// The update async.
        /// </summary>
        /// <param name="key"> The key. </param>
        /// <param name="entity"> The entity. </param>
        /// <returns> The <see cref="Task"/>. </returns>
        Task<T> UpdateAsync(TId key, T entity);

        #endregion
    }
}

