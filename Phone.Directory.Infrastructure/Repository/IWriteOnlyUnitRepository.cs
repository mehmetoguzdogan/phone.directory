﻿using System;
using Phone.Directory.Infrastructure.Entity;

namespace Phone.Directory.Infrastructure.Repository
{
    public interface IWriteOnlyUnitRepository<T, TId> where T : IBaseEntity<TId>
    {
        #region Methods

        /// <summary>
        /// The Add
        /// </summary>
        /// <param name="entities">The entities<see cref="ICollection{T}"/></param>
        void AddUnit(ICollection<T> entities);

        /// <summary>
        /// The Add
        /// </summary>
        /// <param name="entity">The entity<see cref="T"/></param>
        void AddUnit(T entity);

        /// <summary>
        /// The AddAsync
        /// </summary>
        /// <param name="entities">The entities<see cref="IEnumerable{T}"/></param>
        /// <returns>The <see cref="Task{IEnumerable{T}}"/></returns>
        Task AddUnitAsync(ICollection<T> entities);

        /// <summary>
        /// The Delete
        /// </summary>
        /// <param name="entities">The entities<see cref="ICollection{T}"/></param>
        void DeleteUnit(ICollection<T> entities);

        /// <summary>
        /// The Delete
        /// </summary>
        /// <param name="entity">The entity<see cref="T"/></param>
        void DeleteUnit(T entity);

        /// <summary>
        /// The Delete
        /// </summary>
        /// <param name="key">The key<see cref="TId"/></param>
        void DeleteUnit(TId key);

        /// <summary>
        /// The UpdateAsync
        /// </summary>
        /// <param name="entities">The entities<see cref="ICollection{T}"/></param>
        void UpdateUnit(ICollection<T> entities);

        /// <summary>
        /// The UpdateAsync
        /// </summary>
        /// <param name="key">The key<see cref="TId"/></param>
        /// <param name="entity">The entity<see cref="T"/></param>
        void UpdateUnit(TId key, T entity);

        #endregion
    }
}

