﻿using System;
using Phone.Directory.Infrastructure.Entity;

namespace Phone.Directory.Infrastructure.Repository
{
    public class PhoneDirectoryRepository<T, TId> : ReadOnlyRepository<T, TId>, IRepository<T, TId> where T : class, IBaseEntity<TId>
    {
        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="TnRepository{T, TId}"/> class.
        /// </summary>
        /// <param name="context">The context<see cref="IContext"/></param>
        public PhoneDirectoryRepository(IContext context) : base(context)
        {
        }

        #endregion

        #region Methods

        /// <summary>
        /// The Add
        /// </summary>
        /// <param name="entities">The entities<see cref="ICollection{T}"/></param>
        /// <returns>The <see cref="ICollection{T}"/></returns>
        public ICollection<T> Add(ICollection<T> entities)
        {
            if (entities == null || !entities.Any())
            {
                return entities;
            }

            Table.AddRange(entities);
            context.SaveChanges();

            return entities;
        }

        /// <summary>
        /// The Add
        /// </summary>
        /// <param name="entity">The entity<see cref="T"/></param>
        /// <returns>The <see cref="TId"/></returns>
        public TId Add(T entity)
        {
            Table.Add(entity);
            context.SaveChanges();

            return entity.Id;
        }

        /// <summary>
        /// The AddAsync
        /// </summary>
        /// <param name="entities">The entities<see cref="IEnumerable{T}"/></param>
        /// <returns>The <see cref="Task{IEnumerable{T}}"/></returns>
        public async Task<ICollection<T>> AddAsync(ICollection<T> entities)
        {
            if (entities == null || !entities.Any())
            {
                return entities;
            }

            await Table.AddRangeAsync(entities);
            await context.SaveChangesAsync();

            return entities;
        }

        /// <summary>
        /// The AddAsync
        /// </summary>
        /// <param name="entity">The entity<see cref="T"/></param>
        /// <returns>The <see cref="Task{TId}"/></returns>
        public async Task<TId> AddAsync(T entity)
        {
            Table.Add(entity);
            await context.SaveChangesAsync();

            return entity.Id;
        }

        /// <summary>
        /// The Delete
        /// </summary>
        /// <param name="entities">The entities<see cref="ICollection{T}"/></param>
        public void Delete(ICollection<T> entities)
        {
            if (entities == null || !entities.Any())
            {
                return;
            }

            Table.RemoveRange(entities);
            context.SaveChanges();
        }

        /// <summary>
        /// The Delete
        /// </summary>
        /// <param name="entity">The entity<see cref="T"/></param>
        /// <returns>The <see cref="int"/></returns>
        public int Delete(T entity)
        {
            Table.Remove(entity);

            return context.SaveChanges();
        }

        /// <summary>
        /// The Delete
        /// </summary>
        /// <param name="key">The key<see cref="TId"/></param>
        /// <returns>The <see cref="bool"/></returns>
        public bool Delete(TId key)
        {
            var entity = Table.Find(key);

            if (entity == null)
            {
                return false;
            }

            Table.Remove(entity);

            return context.SaveChanges() > 0;
        }

        /// <summary>
        /// The DeleteAsync
        /// </summary>
        /// <param name="entities">The entities<see cref="ICollection{T}"/></param>
        /// <returns>The <see cref="Task"/></returns>
        public async Task DeleteAsync(ICollection<T> entities)
        {
            if (entities == null || !entities.Any())
            {
                return;
            }

            Table.RemoveRange(entities);
            await context.SaveChangesAsync();
        }

        /// <summary>
        /// The DeleteAsync
        /// </summary>
        /// <param name="entity">The entity<see cref="T"/></param>
        /// <returns>The <see cref="Task{int}"/></returns>
        public async Task<int> DeleteAsync(T entity)
        {
            Table.Remove(entity);

            return await context.SaveChangesAsync();
        }

        /// <summary>
        /// The DeleteAsync
        /// </summary>
        /// <param name="key">The key<see cref="TId"/></param>
        /// <returns>The <see cref="Task{bool}"/></returns>
        public async Task<bool> DeleteAsync(TId key)
        {
            var entity = await Table.FindAsync(key);

            if (entity == null)
            {
                return false;
            }

            return await DeleteAsync(entity) > 0;
        }

        /// <summary>
        /// The Update
        /// </summary>
        /// <param name="entities">The entities<see cref="ICollection{T}"/></param>
        /// <returns>The <see cref="ICollection{T}"/></returns>
        public ICollection<T> Update(ICollection<T> entities)
        {
            if (entities == null || !entities.Any())
            {
                return entities;
            }

            Table.UpdateRange(entities);
            context.SaveChanges();

            return entities;
        }

        /// <summary>
        /// The Update
        /// </summary>
        /// <param name="key">The key<see cref="TId"/></param>
        /// <param name="entity">The entity<see cref="T"/></param>
        /// <returns>The <see cref="T"/></returns>
        public T Update(TId key, T entity)
        {
            if (entity == null)
            {
                return null;
            }

            var item = Table.Find(key);

            if (item == null)
            {
                return null;
            }

            Table.Update(entity);
            context.SaveChanges();

            return item;
        }

        /// <summary>
        /// The UpdateAsync
        /// </summary>
        /// <param name="entities">The entities<see cref="ICollection{T}"/></param>
        /// <returns>The <see cref="Task{ICollection{T}}"/></returns>
        public async Task<ICollection<T>> UpdateAsync(ICollection<T> entities)
        {
            if (entities == null || !entities.Any())
            {
                return entities;
            }

            Table.UpdateRange(entities);
            await context.SaveChangesAsync();

            return entities;
        }

        /// <summary>
        /// The UpdateAsync
        /// </summary>
        /// <param name="key">The key<see cref="TId"/></param>
        /// <param name="entity">The entity<see cref="T"/></param>
        /// <returns>The <see cref="Task{T}"/></returns>
        public async Task<T> UpdateAsync(TId key, T entity)
        {
            if (entity == null)
            {
                return null;
            }

            var item = Table.Find(key);

            if (item == null)
            {
                return null;
            }

            Table.Update(entity);
            await context.SaveChangesAsync();

            return item;
        }

        #endregion
    }
}

