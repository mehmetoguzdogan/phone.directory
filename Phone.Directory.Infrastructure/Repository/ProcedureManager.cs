﻿using System;
namespace Phone.Directory.Infrastructure.Repository
{
    public class ProcedureManager : IProcedureManager
    {
        #region Fields

        /// <summary>
        /// Defines the context
        /// </summary>
        private readonly IContext context;

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="ProcedureManager"/> class.
        /// </summary>
        /// <param name="context">The context<see cref="IContext"/></param>
        public ProcedureManager(IContext context) => this.context = context;

        #endregion

        #region Methods

        /// <summary>
        /// The ExecureSqlCommand
        /// </summary>
        /// <param name="sqlQuery">The sqlQuery<see cref="string"/></param>
        /// <param name="timeout">The timeout<see cref="int?"/></param>
        /// <param name="parameters">The parameters<see cref="object[]"/></param>
        /// <returns>The <see cref="int"/></returns>
        public int ExecuteSqlCommand(string sqlQuery, int? timeout = null, params object[] parameters)
            => context.ExecuteSqlCommand(sqlQuery, timeout, parameters);

        #endregion
    }
}

