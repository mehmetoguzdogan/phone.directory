﻿using System;
using System.Collections.Generic;
using Phone.Directory.Infrastructure.Entity;
using Phone.Directory.Infrastructure.Extensions;

namespace Phone.Directory.Infrastructure.Repository
{
    public class UnitPhoneDirectoryRepository<T, TId, TUserId> : ReadOnlyRepository<T, TId>, IUnitRepository<T, TId> where T : class, IEntity<TId, TUserId>
    {
        #region Fields

        /// <summary>
        /// Defines the user
        /// </summary>
        private readonly IUser<TUserId> user;

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="UnitTnRepository{T, TId, TUserId}"/> class.
        /// </summary>
        /// <param name="context">The context<see cref="IUnitContext"/></param>
        /// <param name="user">The user<see cref="IUser{TUserId}"/></param>
        public UnitPhoneDirectoryRepository(IUnitContext context, IUser<TUserId> user) : base(context)
            => this.user = user;

        #endregion

        #region Methods

        /// <summary>
        /// The Add
        /// </summary>
        /// <param name="entities">The entities<see cref="ICollection{T}"/></param>
        public void AddUnit(ICollection<T> entities)
        {
            if (entities == null || !entities.Any())
            {
                return;
            }

            entities.AddEntity<T, TId, TUserId>(user.Id, DateTime.Now);
            Table.AddRange(entities);
        }

        /// <summary>
        /// The Add
        /// </summary>
        /// <param name="entity">The entity<see cref="T"/></param>
        public void AddUnit(T entity)
        {
            entity.AddEntity<T, TId, TUserId>(user.Id, DateTime.Now);
            Table.Add(entity);
        }

        /// <summary>
        /// The AddAsync
        /// </summary>
        /// <param name="entities">The entities<see cref="IEnumerable{T}"/></param>
        /// <returns>The <see cref="Task{IEnumerable{T}}"/></returns>
        public async Task AddUnitAsync(ICollection<T> entities)
        {
            if (entities == null || !entities.Any())
            {
                return;
            }
            entities.AddEntity<T, TId, TUserId>(user.Id, DateTime.Now);
            await Table.AddRangeAsync(entities);
        }

        /// <summary>
        /// The Delete
        /// </summary>
        /// <param name="entities">The entities<see cref="ICollection{T}"/></param>
        public void DeleteUnit(ICollection<T> entities)
        {
            if (entities == null || !entities.Any())
            {
                return;
            }
            entities.SoftDeleteEntity<T, TId, TUserId>(user.Id, DateTime.Now);
            Table.UpdateRange(entities);
        }

        /// <summary>
        /// The Delete
        /// </summary>
        /// <param name="entity">The entity<see cref="T"/></param>
        public void DeleteUnit(T entity)
        {
            entity.SoftDeleteEntity<T, TId, TUserId>(user.Id, DateTime.Now);
            Table.Update(entity);
        }

        /// <summary>
        /// The Delete
        /// </summary>
        /// <param name="key">The key<see cref="TId"/></param>
        public void DeleteUnit(TId key)
        {
            var entity = Table.Find(key);

            if (entity == null)
            {
                return;
            }

            entity.SoftDeleteEntity<T, TId, TUserId>(user.Id, DateTime.Now);
            Table.Update(entity);
        }

        /// <summary>
        /// The UpdateAsync
        /// </summary>
        /// <param name="entities">The entities<see cref="ICollection{T}"/></param>
        public void UpdateUnit(ICollection<T> entities)
        {
            if (entities == null || !entities.Any())
            {
                return;
            }

            entities.UpdateEntity<T, TId, TUserId>(user.Id, DateTime.Now);
            Table.UpdateRange(entities);
        }

        /// <summary>
        /// The UpdateAsync
        /// </summary>
        /// <param name="key">The key<see cref="TId"/></param>
        /// <param name="entity">The entity<see cref="T"/></param>
        public void UpdateUnit(TId key, T entity)
        {
            if (entity == null)
            {
                return;
            }

            var item = Table.Find(key);

            if (item == null)
            {
                return;
            }

            entity.UpdateEntity<T, TId, TUserId>(user.Id, DateTime.Now);
            Table.Update(entity);
        }

        #endregion
    }
}

