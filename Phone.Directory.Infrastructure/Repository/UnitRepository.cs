﻿using System;
using Phone.Directory.Infrastructure.Entity;

namespace Phone.Directory.Infrastructure.Repository
{
    public class UnitRepository<T, TId> : ReadOnlyRepository<T, TId>, IUnitRepository<T, TId> where T : class, IBaseEntity<TId>
    {
        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="UnitRepository{T, TId}"/> class.
        /// </summary>
        /// <param name="context">The context<see cref="IUnitContext"/></param>
        public UnitRepository(IUnitContext context) : base(context)
        {
        }

        #endregion

        #region Methods

        /// <summary>
        /// The Add
        /// </summary>
        /// <param name="entities">The entities<see cref="ICollection{T}"/></param>
        public void AddUnit(ICollection<T> entities)
        {
            if (entities == null || !entities.Any())
            {
                return;
            }

            Table.AddRange(entities);
        }

        /// <summary>
        /// The Add
        /// </summary>
        /// <param name="entity">The entity<see cref="T"/></param>
        public void AddUnit(T entity)
            => Table.Add(entity);

        /// <summary>
        /// The AddAsync
        /// </summary>
        /// <param name="entities">The entities<see cref="IEnumerable{T}"/></param>
        /// <returns>The <see cref="Task{IEnumerable{T}}"/></returns>
        public async Task AddUnitAsync(ICollection<T> entities)
        {
            if (entities == null || !entities.Any())
            {
                return;
            }

            await Table.AddRangeAsync(entities);
        }

        /// <summary>
        /// The Delete
        /// </summary>
        /// <param name="entities">The entities<see cref="ICollection{T}"/></param>
        public void DeleteUnit(ICollection<T> entities)
        {
            if (entities == null || !entities.Any())
            {
                return;
            }

            Table.RemoveRange(entities);
        }

        /// <summary>
        /// The Delete
        /// </summary>
        /// <param name="entity">The entity<see cref="T"/></param>
        public void DeleteUnit(T entity)
            => Table.Remove(entity);

        /// <summary>
        /// The Delete
        /// </summary>
        /// <param name="key">The key<see cref="TId"/></param>
        public void DeleteUnit(TId key)
        {
            var entity = Table.Find(key);

            if (entity == null)
            {
                return;
            }

            Table.Remove(entity);
        }

        /// <summary>
        /// The UpdateAsync
        /// </summary>
        /// <param name="entities">The entities<see cref="ICollection{T}"/></param>
        public void UpdateUnit(ICollection<T> entities)
        {
            if (entities == null || !entities.Any())
            {
                return;
            }

            Table.UpdateRange(entities);
        }

        /// <summary>
        /// The UpdateAsync
        /// </summary>
        /// <param name="key">The key<see cref="TId"/></param>
        /// <param name="entity">The entity<see cref="T"/></param>
        public void UpdateUnit(TId key, T entity)
        {
            if (entity == null)
            {
                return;
            }

            var item = Table.Find(key);

            if (item == null)
            {
                return;
            }

            Table.Update(entity);
        }

        #endregion
    }
}

