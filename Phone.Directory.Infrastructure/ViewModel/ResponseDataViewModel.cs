﻿using System;
namespace Phone.Directory.Infrastructure.ViewModel
{
    public class ResponseDataViewModel<T>
    {
        #region Properties

        /// <summary>
        /// Gets or sets the Code.
        /// </summary>
        public int Code { get; set; }

        /// <summary>
        /// Gets or sets the Data.
        /// </summary>
        public T Data { get; set; }

        /// <summary>
        /// Gets or sets the Message.
        /// </summary>
        public string Message { get; set; }

        #endregion
    }
}

