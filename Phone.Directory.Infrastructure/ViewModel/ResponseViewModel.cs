﻿using System;
namespace Phone.Directory.Infrastructure.ViewModel
{
    public class ResponseViewModel
    {
        #region Properties

        /// <summary>
        /// Gets or sets the Code.
        /// </summary>
        public int Code { get; set; }

        /// <summary>
        /// Gets or sets the Message.
        /// </summary>
        public string Message { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether Result
        /// Gets or sets the Data..
        /// </summary>
        public bool Result { get; set; }

        #endregion
    }
}

