﻿using System;
using Microsoft.Extensions.DependencyInjection;
using Phone.Directory.Infrastructure.Engine;
using Phone.Directory.Repository;
using Phone.Directory.Infrastructure.Logs;

namespace Phone.Directory.Repository.Engine
{
    public class PosgreRegister : IDynamicRegister
    {
        #region Methods

        /// <summary>
        /// The Configure.
        /// </summary>
        /// <param name="service">The service<see cref="IServiceCollection"/>.</param>
        public void Configure(IServiceCollection service)
        {
            service.AddTransient<IPhoneDirectoryContext>(f => { return new PhoneDirectoryContext(f.GetService<ConsoleLoggerFactory>().LoggerFactory); });
            service.AddScoped(typeof(IPhoneDirectoryRepository<,,>), typeof(PhoneDirectoryRepository<,,>));
            service.AddScoped(typeof(IPhoneDirectoryPdRepository<,>), typeof(PhoneDirectoryPdRepository<,>));
            service.AddScoped(typeof(IProcedureManagerPhoneDirectory), typeof(ProcedureManagerPhoneDirectory));
            service.AddEntityFrameworkNpgsql();
        }

        #endregion
    }
}

