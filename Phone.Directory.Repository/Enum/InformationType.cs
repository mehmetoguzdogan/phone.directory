﻿using System;
using Phone.Directory.Infrastructure.Attributes;
using System.ComponentModel;

namespace Phone.Directory.Repository.Enum
{
    public enum InformationType
    {
        [Description("TelefonNumarası")]
        PhoneNumber,
        [Description("Eposta")]
        Email,
        [Description("Konum")]
        Location
    }
}

