﻿using System;
using Phone.Directory.Infrastructure.Entity;
using Phone.Directory.Infrastructure.Repository;

namespace Phone.Directory.Repository
{
    public interface IPhoneDirectoryPdRepository<T, TId> : IRepository<T, TId> where T : IBaseEntity<TId>
    {
    }
}

