﻿using System;
using Phone.Directory.Infrastructure.Entity;
using Phone.Directory.Infrastructure.Repository;

namespace Phone.Directory.Repository
{
    public interface IPhoneDirectoryRepository<T, TId, TUserId> : IRepository<T, TId> where T : IEntity<TId, TUserId>
    {
    }
}

