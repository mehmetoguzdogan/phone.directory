﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Phone.Directory.Repository.Model;

namespace Phone.Directory.Repository.Map
{
    public class ContactInformationMap : IEntityTypeConfiguration<ContactInformation>
    {
        #region Methods

        /// <summary>
        /// The Configure.
        /// </summary>
        /// <param name="builder">The builder<see cref="EntityTypeBuilder{FreezeModel}"/>.</param>
        public void Configure(EntityTypeBuilder<ContactInformation> builder)
        {
            //Keys
            builder.HasKey(x => x.Id);

            //Table
            builder.ToTable("contactinformation");

            //Properties
            builder.Property(x => x.Id).HasColumnName("id").IsRequired();
            builder.Property(x => x.InformationType).HasColumnName("informationtype").IsRequired();
            builder.Property(x => x.InformationValue).HasColumnName("informationvalue").IsRequired();
            builder.Property(x => x.CreateDate).HasColumnName("createdate").IsRequired();
            builder.Property(x => x.UpdateDate).HasColumnName("updatedate");
            builder.Property(x => x.IsActive).HasColumnName("isactive").IsRequired();
            builder.Property(x => x.PersonId).HasColumnName("personid").IsRequired();
        }

        #endregion
    }
}

