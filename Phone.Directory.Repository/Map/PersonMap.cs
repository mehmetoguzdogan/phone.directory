﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Phone.Directory.Repository.Model;

namespace Phone.Directory.Repository.Map
{
    public class PersonMap : IEntityTypeConfiguration<Person>
    {
        #region Methods

        /// <summary>
        /// The Configure.
        /// </summary>
        /// <param name="builder">The builder<see cref="EntityTypeBuilder{FreezeModel}"/>.</param>
        public void Configure(EntityTypeBuilder<Person> builder)
        {
            //Keys
            builder.HasKey(x => x.Id);

            //Table
            builder.ToTable("person");

            //Properties
            builder.Property(x => x.Id).HasColumnName("id").IsRequired();
            builder.Property(x => x.Name).HasColumnName("name").IsRequired();
            builder.Property(x => x.LastName).HasColumnName("lastname").IsRequired();
            builder.Property(x => x.Company).HasColumnName("company");
            builder.Property(x => x.CreateDate).HasColumnName("createdate").IsRequired();
            builder.Property(x => x.UpdateDate).HasColumnName("updatedate");
            builder.Property(x => x.IsActive).HasColumnName("isactive").IsRequired();
        }

        #endregion
    }
}

