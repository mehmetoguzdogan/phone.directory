﻿using System;
using Phone.Directory.Infrastructure.Entity;

namespace Phone.Directory.Repository.Model
{
    public class ContactInformation: IBaseEntity<Guid>
    {
        public Guid Id { get; set; }
        public Guid PersonId { get; set; }
        public int InformationType { get; set; }
        public string InformationValue { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime? UpdateDate { get; set; }
        public int IsActive { get; set; }
    }
}

