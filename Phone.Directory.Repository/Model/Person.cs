﻿using System;
using Phone.Directory.Infrastructure.Entity;

namespace Phone.Directory.Repository.Model
{
    public class Person  : IBaseEntity<Guid>
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string LastName { get; set; }
        public string Company { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime? UpdateDate { get; set; }
        public int IsActive { get; set; }
    }
}

