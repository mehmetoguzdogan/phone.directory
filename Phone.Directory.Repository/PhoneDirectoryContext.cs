﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Phone.Directory.Infrastructure.DbContext;
using Phone.Directory.Repository.Const;
using Phone.Directory.Repository.Map;
using Phone.Directory.Repository.Model;

namespace Phone.Directory.Repository
{
    public class PhoneDirectoryContext : PostgreDbContext, IPhoneDirectoryContext
    {
        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="PhoneDirectoryContext"/> class.
        /// </summary>
        /// <param name="loggerFactory">The loggerFactory<see cref="ILoggerFactory"/>.</param>
        public PhoneDirectoryContext(ILoggerFactory loggerFactory) : base(ConnectionConst.PosgreConnection, loggerFactory, "public")
        {
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the Person.
        /// </summary>
        public DbSet<Person> Persons { get; set; }

        /// <summary>
        /// Gets or sets the ContactInformation.
        /// </summary>
        public DbSet<ContactInformation> ContactInformations { get; set; }

        #endregion

        #region Methods

        /// <summary>
        /// The OnModelCreating.
        /// </summary>
        /// <param name="modelBuilder">The modelBuilder<see cref="ModelBuilder"/>.</param>
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.ApplyConfiguration(new PersonMap());
            modelBuilder.ApplyConfiguration(new ContactInformationMap());
        }

        #endregion
    }
}

