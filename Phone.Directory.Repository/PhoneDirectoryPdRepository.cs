﻿using System;
using Phone.Directory.Infrastructure.Entity;
using Phone.Directory.Infrastructure.Repository;

namespace Phone.Directory.Repository
{
    public class PhoneDirectoryPdRepository<T, TId> : PhoneDirectoryRepository<T, TId>, IPhoneDirectoryPdRepository<T, TId> where T : class, IBaseEntity<TId>
    {
        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="PhoneDirectoryPdRepository{T, TId}"/> class.
        /// </summary>
        /// <param name="PhoneDirectoryContext">The TnProductFreezeContext<see cref="IPhoneDirectoryContext"/>.</param>
        public PhoneDirectoryPdRepository(IPhoneDirectoryContext PhoneDirectoryContext) : base((IContext)PhoneDirectoryContext)
        {
        }

        #endregion
    }
}

