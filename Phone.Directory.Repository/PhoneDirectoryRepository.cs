﻿using System;
using Phone.Directory.Infrastructure.Entity;
using Phone.Directory.Infrastructure.Repository;

namespace Phone.Directory.Repository
{
    public class PhoneDirectoryRepository<T, TId, TUserId> : Repository<T, TId, TUserId>, IPhoneDirectoryRepository<T, TId, TUserId> where T : class, IEntity<TId, TUserId>
    {
        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="PhoneDirectoryRepository{T, TId, TUserId}"/> class.
        /// </summary>
        /// <param name="context">The context<see cref="IPhoneDirectoryContext"/>.</param>
        /// <param name="user">The user<see cref="IUser{TUserId}"/>.</param>
        public PhoneDirectoryRepository(IPhoneDirectoryContext context, IUser<TUserId> user) : base(context, user)
        {
        }

        #endregion
    }
}

