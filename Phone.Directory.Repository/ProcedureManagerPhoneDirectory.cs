﻿using System;
using Phone.Directory.Infrastructure.Repository;

namespace Phone.Directory.Repository
{
    public class ProcedureManagerPhoneDirectory : ProcedureManager, IProcedureManagerPhoneDirectory
    {
        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="ProcedureManagerPhoneDirectory"/> class.
        /// </summary>
        /// <param name="context">The context<see cref="IPhoneDirectoryContext"/>.</param>
        public ProcedureManagerPhoneDirectory(IPhoneDirectoryContext context) : base(context)
        {
        }

        #endregion
    }
}

