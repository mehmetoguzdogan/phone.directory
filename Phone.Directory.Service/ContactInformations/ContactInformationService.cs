﻿using System;
using Phone.Directory.Repository;
using Phone.Directory.Repository.Model;

namespace Phone.Directory.Service.ContactInformations
{
    public class ContactInformationService : IContactInformationService
    {
        private readonly IPhoneDirectoryPdRepository<ContactInformation, Guid> _contactInformationRepository;

        public ContactInformationService(IPhoneDirectoryPdRepository<ContactInformation, Guid> contactInformationRepository)
        {
            _contactInformationRepository = contactInformationRepository;
        }

        public Guid Add(ContactInformation contactInformation)
        {
            contactInformation.CreateDate = DateTime.SpecifyKind(DateTime.Now, DateTimeKind.Utc);
            contactInformation.IsActive = default(int);
            return _contactInformationRepository.Add(contactInformation);
        }

        public bool Delete(Guid id)
        {
            return _contactInformationRepository.Delete(id);
        }

        public ContactInformation Get(Guid id)
        {
            return _contactInformationRepository.Find(x=>x.Id == id && x.IsActive == 1);
        }

        public List<ContactInformation> GetList()
        {
            return _contactInformationRepository.FindAll(x => x.IsActive == 1).ToList();
        }

        public ContactInformation Update(Guid id, ContactInformation contactInformation)
        {
            contactInformation.CreateDate = DateTime.SpecifyKind(contactInformation.CreateDate, DateTimeKind.Utc);
            contactInformation.UpdateDate = DateTime.SpecifyKind(DateTime.Now, DateTimeKind.Utc);
            return _contactInformationRepository.Update(contactInformation.Id, contactInformation);
        }
    }
}

