﻿using System;
using Phone.Directory.Repository.Model;

namespace Phone.Directory.Service.ContactInformations
{
    public interface IContactInformationService
    {

        Guid Add(ContactInformation contactInformation);
        ContactInformation Update(Guid id, ContactInformation contactInformation);
        bool Delete(Guid id);
        ContactInformation Get(Guid id);
        List<ContactInformation> GetList();
    }
}

