﻿using System;
using Phone.Directory.Repository.Model;

namespace Phone.Directory.Service.Persons
{
    public interface IPersonService
    {
        Guid Add(Person person);
        Person Update(Guid id,Person person);
        bool Delete(Guid id);
        Person Get(Guid id);
        List<Person> GetList();
    }
}

