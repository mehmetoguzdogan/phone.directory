﻿using Dapper;
using System.Data;
using Microsoft.Data.SqlClient;
using Phone.Directory.Repository.Const;
using Phone.Directory.Repository.Model;
using Phone.Directory.Repository;

namespace Phone.Directory.Service.Persons
{
    public class PersonService : IPersonService
    {
       private readonly IPhoneDirectoryPdRepository<Person,Guid> _phoneDirectoryRepository;

        public PersonService(IPhoneDirectoryPdRepository<Person, Guid> phoneDirectoryRepository)
        {
            _phoneDirectoryRepository = phoneDirectoryRepository;
        }

        public Guid Add(Person person)
        {
           person.CreateDate = DateTime.SpecifyKind(DateTime.Now,DateTimeKind.Utc);
            person.IsActive = default(int);
           return _phoneDirectoryRepository.Add(person);
        }

        public bool Delete(Guid id)
        {
           return _phoneDirectoryRepository.Delete(id);
        }

        public Person Get(Guid id)
        {
            return _phoneDirectoryRepository.Find(x=>x.Id == id && x.IsActive == 1);
        }

        public List<Person> GetList()
        {
            return _phoneDirectoryRepository.FindAll(x => x.IsActive == 1).ToList();
        }

        public Person Update(Guid id, Person person)
        { 
            person.CreateDate = DateTime.SpecifyKind(person.CreateDate, DateTimeKind.Utc);
            person.UpdateDate = DateTime.SpecifyKind(DateTime.Now, DateTimeKind.Utc);
            return _phoneDirectoryRepository.Update(id,person);
        }
    }
}

