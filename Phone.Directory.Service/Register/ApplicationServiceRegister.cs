﻿using System;
using Microsoft.Extensions.DependencyInjection;
using Phone.Directory.Infrastructure.Engine;
using Phone.Directory.Service.ContactInformations;
using Phone.Directory.Service.Persons;
using Phone.Directory.Service.Report;

namespace Phone.Directory.Service.Register
{
    public class ApplicationServiceRegister : IDynamicRegister
    {
        #region Methods

        /// <summary>
        /// The Configure.
        /// </summary>
        /// <param name="service">The service<see cref="IServiceCollection"/>.</param>
        public void Configure(IServiceCollection service)
        {
            service.AddTransient<IPersonService, PersonService>();
            service.AddTransient<IContactInformationService, ContactInformationService>();
            service.AddTransient<IReportService, ReportService>();
        }

        #endregion
    }
}

