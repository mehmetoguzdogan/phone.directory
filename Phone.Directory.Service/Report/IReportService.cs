﻿using System;
using Phone.Directory.Service.Report.Model;

namespace Phone.Directory.Service.Report
{
    public interface IReportService
    {
        void GetReportInfos(Guid reportRequestId);
    }
}

