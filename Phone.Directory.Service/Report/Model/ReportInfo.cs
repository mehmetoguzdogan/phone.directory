﻿using System;
namespace Phone.Directory.Service.Report.Model
{
    public class ReportInfo
    {
        public Guid Id { get; set; }
        public Guid RequestId { get; set; }
        public string Location { get; set; }
        public int PersonCount { get; set; }
        public int PhoneNumberCount { get; set; }
        public int IsActive { get; set; }
    }
}

