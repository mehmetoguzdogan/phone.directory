﻿using System;
using Phone.Directory.Repository.Enum;
using Phone.Directory.Repository.Model;
using Phone.Directory.Service.ContactInformations;
using Phone.Directory.Service.Report.Model;
using RestSharp;

namespace Phone.Directory.Service.Report
{
    public class ReportService : IReportService
    {
        private readonly IContactInformationService _contactInformationService;
        public ReportService(IContactInformationService contactInformationService)
        {
            _contactInformationService = contactInformationService;
        }

        public async void GetReportInfos(Guid reportRequestId)
        {
            List<ContactInformation> informations = _contactInformationService.GetList();

            RestClient _client = new RestClient("http://localhost:5154/");
            var request = new RestRequest($"api/report?id={reportRequestId}&statu={(int)ReportStatu.preparing}");
            var response = _client.ExecutePut<Guid>(request);
            _client.Dispose();

            List<ReportInfo> reportInfos = new List<ReportInfo>();


            var personLocationAndPhone = (from location in informations.Where(x=>x.InformationType == (int)InformationType.Location).ToList()
                                            join phone in informations.Where(x=>x.InformationType == (int)InformationType.PhoneNumber).ToList()
                                            on location.PersonId equals phone.PersonId
                                            into temp from t in temp.DefaultIfEmpty()
                                          select new
                                            {
                                                location = location.InformationValue,
                                                phone= t?.InformationValue ?? string.Empty,
                                                personId = location.PersonId
                                            }).ToList();

            foreach (var item in informations.Where(x=>x.InformationType == (int)InformationType.Location).GroupBy(x=>x.InformationValue))
            {
                ReportInfo report = new ReportInfo()
                {
                    IsActive = 0,
                    RequestId = reportRequestId,
                    Location = item.Key,
                    PersonCount = personLocationAndPhone.Where(x => x.location == item.Key).Count(),
                    PhoneNumberCount = personLocationAndPhone.Where(x=>x.location == item.Key).Count(x=>!string.IsNullOrEmpty(x.phone))
                };
                reportInfos.Add(report);
            }

            _client = new RestClient("http://localhost:5154/");
            var request1 = new RestRequest("api/report/ReportsAdd")
                  .AddJsonBody(reportInfos);
            var response1 = await _client.ExecutePostAsync(request1);
            _client.AddDefaultHeader("ContentType","application/json");
            _client.Dispose();

           // response1.StatusCode == System.Net.HttpStatusCode.OK

            _client = new RestClient("http://localhost:5154/");
            var request2 = new RestRequest($"api/report?id={reportRequestId}&statu={(int)ReportStatu.prepared}");
            var response2 = _client.ExecutePut<Guid>(request2);
            _client.Dispose();

        }
    }
}

