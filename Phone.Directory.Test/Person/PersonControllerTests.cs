﻿using System;
using AutoFixture;
using Moq;
using Shouldly;
using Xunit;
using Phone.Directory.Repository.Model;
using Phone.Directory.Service.Persons;
using Phone.Directory.Repository;
using Phone.Directory.Api.Controllers;
using Microsoft.AspNetCore.Mvc;
using Phone.Directory.Infrastructure.Entity;

namespace Phone.Directory.Test.Person
{
    public class PersonControllerTests
    {
        public readonly IPersonService mockPersonService;
        public PersonControllerTests()
        {
            // Test metotları genelinde kullanacağımız User listesi
            var userList = new List<Phone.Directory.Repository.Model.Person>
            {
                new Phone.Directory.Repository.Model.Person {Id=Guid.Parse("4451A4BA-0945-454E-84DB-7D62C2ECB026"),Name="oguz",LastName="dogan" , Company = "test1", IsActive = 0, CreateDate = DateTime.Now },
                new Phone.Directory.Repository.Model.Person {Id=Guid.Parse("375A6691-8A7F-4B3B-9946-A6C3CE352609"),Name="ayşe",LastName="dogan" ,Company = "test2", IsActive = 0, CreateDate = DateTime.Now  },
                new Phone.Directory.Repository.Model.Person {Id=Guid.Parse("F2334898-DF7E-41A8-91CE-06BBB3B37582"),Name="ali",LastName="dogan" ,Company = "test3", IsActive = 0, CreateDate = DateTime.Now  }
            };
            // Mock the Products Repository using Moq
            var mockPersonService = new Mock<IPersonService>();
            // GetAll metodu için setup işlemi
            mockPersonService.Setup(mr => mr.GetList()).Returns(userList);
            // GetById metodu için setup işlemi
            mockPersonService.Setup(mr => mr.Get(It.IsAny<Guid>())).Returns((Guid i) => userList.Single(x => x.Id == i));
            // Insert için setup işlemi
            mockPersonService.Setup(mr => mr.Add(It.IsAny<Phone.Directory.Repository.Model.Person>())).Callback(
                (Phone.Directory.Repository.Model.Person target) =>
                {
                    userList.Add(target);
                });
            // Update için setup işlemi
            mockPersonService.Setup(mr => mr.Update(It.IsAny<Guid>(),It.IsAny<Phone.Directory.Repository.Model.Person>())).Callback(
                (Guid guid,Phone.Directory.Repository.Model.Person target) =>
                {
                    var original = userList.Where(q => q.Id == target.Id).FirstOrDefault();
                    if (original == null)
                    {
                        throw new InvalidOperationException();
                    }
                    original.Company = target.Company;
                    original.IsActive = target.IsActive;
                });
            // Test metotlarından erişilebilmesi için global olarak tanımladığımız MockUserRepository'e yukarıdaki setup işlemlerini atıyoruz
            this.mockPersonService = mockPersonService.Object;
        }

        [Fact]
        public void ValidPerson_AddIsCalled()
        {
            // Arrange
            var actual = this.mockPersonService.GetList().Count + 1;
            var person = new Phone.Directory.Repository.Model.Person { Id = Guid.Parse("8362B02D-9F90-4DB1-AC4A-C63E2C8EAC7E"), Name = "User4", LastName = "User4LastName", Company = "test1", IsActive = 0, CreateDate = DateTime.Now };
            this.mockPersonService.Add(person);
            var expected = this.mockPersonService.GetList().Count;
            Assert.Equal(actual, expected);
        }

        [Fact]
        public void ValidPerson_GetByIdCalled()
        {
            var actual = new Phone.Directory.Repository.Model.Person { Id = Guid.Parse("4451A4BA-0945-454E-84DB-7D62C2ECB026"), Name = "oguz", LastName = "dogan", Company = "test1", IsActive = 0, CreateDate = DateTime.Now };
            var expected = this.mockPersonService.Get(Guid.Parse("4451A4BA-0945-454E-84DB-7D62C2ECB026"));
            Assert.NotNull(expected); // Test is not null
            Assert.IsType<Phone.Directory.Repository.Model.Person>( expected); // Test type
            Assert.Equal(actual.Id, expected.Id); // test correct object found

        }

        [Fact]
        public void ValidPerson_GetCalled()
        {
            var expected = this.mockPersonService.GetList();
            Assert.NotNull(expected); // test not null
            Assert.IsType<List<Phone.Directory.Repository.Model.Person>>(expected);// test GetAll returns user objects
        }

        [Fact]
        public void ValidPerson_UpdateIsCalled()
        {
            // Arrange

            var actual = new Phone.Directory.Repository.Model.Person { Id = Guid.Parse("4451A4BA-0945-454E-84DB-7D62C2ECB026"), Name = "oguz", LastName = "dogan2", Company = "test3", IsActive = 0, CreateDate = DateTime.Now };
            this.mockPersonService.Update(actual.Id ,actual);
            var expected = this.mockPersonService.Get(actual.Id);
            Assert.NotNull(expected);
            Assert.Equal(actual.Company, expected.Company);
            Assert.Equal(actual.IsActive, expected.IsActive);
        }

        [Fact]
        public void ValidPerson_DeleteIsCalled()
        {
            // Arrange
            var actual = new Phone.Directory.Repository.Model.Person { Id = Guid.Parse("375A6691-8A7F-4B3B-9946-A6C3CE352609"), Name = "ayşe", LastName = "dogan", Company = "test5", IsActive = 0, CreateDate = DateTime.Now };
            this.mockPersonService.Delete(actual.Id);
            var expected = this.mockPersonService.Get(actual.Id);
            Assert.NotEqual(actual, expected);
        }

    }
}

