

PostgreSqlde projeyi actıgınızda 
Phone.Directory.Repository.Const altında bulunan ConnectionConst'taki 
connection string bilgisi degıstırılecek ve daha sonrasında asagıda bulunan ıkı tabloyu postgre'de olusturulması gerekıyor. 

ek olarak rabbitmq ayarları ıcın 
Report.Queuing.Model.Const altında bulunan QueueSettingsConst bilgilerinin guncellenmesi gerekıyor. 

---------------------------
-- Table: public.contactinformation

-- DROP TABLE IF EXISTS public.contactinformation;

CREATE TABLE IF NOT EXISTS public.contactinformation
(
    id uuid NOT NULL DEFAULT uuid_generate_v4(),
    informationtype integer NOT NULL,
    informationvalue text COLLATE pg_catalog."default" NOT NULL,
    createdate date,
    updatedate date,
    isactive integer NOT NULL DEFAULT 1,
    personid uuid NOT NULL
)

TABLESPACE pg_default;

ALTER TABLE IF EXISTS public.contactinformation
    OWNER to postgres;

-----------------------------

-- Table: public.person

-- DROP TABLE IF EXISTS public.person;

CREATE TABLE IF NOT EXISTS public.person
(
    id uuid NOT NULL DEFAULT uuid_generate_v4(),
    name text COLLATE pg_catalog."default" NOT NULL,
    lastname text COLLATE pg_catalog."default" NOT NULL,
    company text COLLATE pg_catalog."default" NOT NULL,
    createdate date NOT NULL,
    updatedate date,
    isactive integer NOT NULL DEFAULT 1
)

TABLESPACE pg_default;

ALTER TABLE IF EXISTS public.person
    OWNER to postgres;


-----------------------------


Proje calısmaya basladıgında RabbitHostedService adında bulunan HostedService rabbitmq consumerını calıstırıyor. 
Bu consumer Report projesınden kuyruga eklenmıs rapor taleplerını alıp ReportService ıcınde bulunan metot ile islemeye baslıyor.
Metotta işleme basladıgı sırada report servisine istek atarak ıslem yaptıgı raporun durumunu ıslenıyor olarak guncellıyor.
rapor hazırlanıp report servisine kayıtları aktarıyor. bu kayıtlar aktarıldıktan sonra raporun durumunu tamamlandı olarak guncellıyor. 

PersonController ve ContactInformationController ıcınde de
yukarıda eklenen tablolara kayıt ekleme listeleme guncelleme silme metotları bulunuyor. 


