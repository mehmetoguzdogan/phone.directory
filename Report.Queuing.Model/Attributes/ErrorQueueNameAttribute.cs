﻿using System;
namespace Report.Queuing.Model.Attributes
{
    public class ErrorQueueNameAttribute
    {
        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="ErrorQueueNameAttribute"/> class.
        /// </summary>
        /// <param name="name">The exchangeName<see cref="string"/></param>
        public ErrorQueueNameAttribute(string name) => Name = name;

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the Name
        /// </summary>
        public string Name { get; set; }

        #endregion
    }
}

