﻿using System;
namespace Report.Queuing.Model.Attributes
{
    public class ExchangeNameAttribute : Attribute
    {
        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="ExchangeNameAttribute"/> class.
        /// </summary>
        /// <param name="exchangeName">The exchangeName<see cref="string"/></param>
        public ExchangeNameAttribute(string exchangeName) => ExchangeName = exchangeName;

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the ExchangeName
        /// </summary>
        public string ExchangeName { get; set; }

        #endregion
    }
}

