﻿using System;
namespace Report.Queuing.Model.Attributes
{
    [Serializable]
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = false)]
    public class QueueNameAttribute : Attribute
    {
        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="QueueNameAttribute"/> class.
        /// </summary>
        /// <param name="queueName">The queueName<see cref="string"/></param>
        public QueueNameAttribute(string queueName) => QueueName = queueName;

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the QueueName
        /// </summary>
        public string QueueName { get; set; }

        #endregion
    }
}

