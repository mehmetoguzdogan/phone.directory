﻿
using EasyNetQ;
using System;
using System.Linq;
using Report.Queuing.Model.Attributes;

namespace Report.Queuing
{
    [Serializable]
    public class AttributeBasedConventions : Conventions
    {
        #region Fields

        /// <summary>
        /// Defines the typeNameSerializer
        /// </summary>
        private readonly ITypeNameSerializer typeNameSerializer;

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="AttributeBasedConventions"/> class.
        /// </summary>
        /// <param name="typeNameSerializer">The typeNameSerializer<see cref="ITypeNameSerializer"/></param>
        public AttributeBasedConventions(ITypeNameSerializer typeNameSerializer)
            : base(typeNameSerializer)
        {
            this.typeNameSerializer = typeNameSerializer ?? throw new ArgumentNullException("typeNameSerializer");
            base.ExchangeNamingConvention = GenerateExchangeName;
            base.QueueNamingConvention = GenerateQueueName;
        }

        #endregion

        #region Methods

        /// <summary>
        /// The GenerateExchangeName
        /// </summary>
        /// <param name="messageType">The messageType<see cref="Type"/></param>
        /// <returns>The <see cref="string"/></returns>
        private string GenerateExchangeName(Type messageType)
        {
            var exchangeNameAtt = messageType.GetCustomAttributes(typeof(ExchangeNameAttribute), true).SingleOrDefault() as ExchangeNameAttribute;
            return (exchangeNameAtt == null) ? typeNameSerializer?.Serialize(messageType) : exchangeNameAtt.ExchangeName;
        }

        /// <summary>
        /// The GenerateQueueName
        /// </summary>
        /// <param name="messageType">The messageType<see cref="Type"/></param>
        /// <param name="subscriptionId">The subscriptionId<see cref="string"/></param>
        /// <returns>The <see cref="string"/></returns>
        private string GenerateQueueName(Type messageType, string subscriptionId)
        {
            var queueNameAtt = messageType?.GetCustomAttributes(typeof(QueueNameAttribute), true)
                .SingleOrDefault() as QueueNameAttribute;

            if (queueNameAtt == null)
            {
                var typeName = typeNameSerializer?.Serialize(messageType);
                return $"{typeName}_{subscriptionId}";
            }

            return string.IsNullOrWhiteSpace(subscriptionId)
                ? queueNameAtt.QueueName : string.Concat(queueNameAtt.QueueName, "_", subscriptionId);
        }

        #endregion
    }
}

