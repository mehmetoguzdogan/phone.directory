﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EasyNetQ;
using EasyNetQ.MessageVersioning;
using EasyNetQ.Topology;
using RabbitMQ.Client;
using Report.Queuing.Extension;
using Report.Queuing.Model.Attributes;
using Report.Queuing.Model.Const;

namespace Report.Queuing
{
    public class BusFactory
    {
        #region Constants

        /// <summary>
        /// Defines the CONSUMER_BUS_KEY
        /// </summary>
        private const string CONSUMER_BUS_KEY = "consumer_";

        /// <summary>
        /// Defines the ERROR_CONSUMER_BUS_KEY
        /// </summary>
        private const string ERROR_CONSUMER_BUS_KEY = "error_consumer_";

        /// <summary>
        /// Defines the PUBLISHER_BUS_KEY
        /// </summary>
        private const string PUBLISHER_BUS_KEY = "publisher_";

        #endregion

        #region Fields

        /// <summary>
        /// Defines the busFactory
        /// </summary>
        private static volatile BusFactory busFactory;

        /// <summary>
        /// Defines the busList
        /// </summary>
        private static volatile IDictionary<string, IBus> busList;

        /// <summary>
        /// Defines the lockBusObject
        /// </summary>
        private static volatile object lockBusObject;

        /// <summary>
        /// Defines the lockObject
        /// </summary>
        private static readonly object lockObject = new();

        #endregion

        #region Constructors

        /// <summary>
        /// Prevents a default instance of the <see cref="BusFactory"/> class from being created.
        /// </summary>
        private BusFactory()
        {
            lockBusObject = new object();
            busList = new Dictionary<string, IBus>();
        }

        #endregion

        #region Delegates

        /// <summary>
        /// The test
        /// </summary>
        /// <param name="info">The info<see cref="MessageReceivedInfo"/></param>
        /// <returns>The <see cref="string"/></returns>
        public delegate string test(MessageReceivedInfo info);

        #endregion

        #region Methods

        /// <summary>
        /// The GetBus
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="consumer">The consumer<see cref="bool"/></param>
        /// <param name="prefetchCount">The prefetchCount<see cref="int"/></param>
        /// <param name="exchangeType">The exchangeType<see cref="string"/></param>
        /// <param name="routingKey">The routingKey<see cref="string"/></param>
        /// <returns>The <see cref="IBus"/></returns>
        public static IBus GetBus<T>(bool consumer, int prefetchCount = 0, string exchangeType = EasyNetQ.Topology.ExchangeType.Topic, string routingKey = "") where T : class
        {
            string busName = string.Concat((consumer ? CONSUMER_BUS_KEY : PUBLISHER_BUS_KEY), typeof(T).ToString());
            GenerateBusFactory();
            if (!busList.Any(x => x.Key.Equals(busName)))
            {
                lock (lockBusObject)
                {
                    if (!busList.Any(x => x.Key.Equals(busName)))
                    {
                        busList.Add(busName, CreateBus<T>(exchangeType, routingKey, prefetchCount));
                    }
                }
            }

            return busList.First(x => x.Key.Equals(busName)).Value;
        }

        /// <summary>
        /// The GetErrorBus
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="consumer"></param>
        /// <param name="prefetchCount"></param>
        /// <param name="exchangeType"></param>
        /// <param name="routingKey"></param>
        /// <returns>.</returns>
        public static IBus GetErrorBus<T>(int prefetchCount = 0) where T : class
        {
            string busName = string.Concat((ERROR_CONSUMER_BUS_KEY), typeof(T).ToString());

            GenerateBusFactory();

            if (!busList.Any(x => x.Key.Equals(busName)))
            {
                lock (lockBusObject)
                {
                    if (!busList.Any(x => x.Key.Equals(busName)))
                    {
                        busList.Add(busName, CreateErrorBus<T>(prefetchCount));
                    }
                }
            }

            return busList.First(x => x.Key.Equals(busName)).Value;
        }

        /// <summary>
        /// The GenerateBusFactory
        /// </summary>
        /// <returns>The <see cref="BusFactory"/></returns>
        private static BusFactory GenerateBusFactory()
        {
            if (busFactory == null)
            {
                lock (lockObject)
                {
                    if (busFactory == null)
                    {
                        busFactory = new BusFactory();
                    }
                }
            }

            return busFactory;
        }

        /// <summary>
        /// The CreateBus
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="exchangeType">The exchangeType<see cref="string"/></param>
        /// <param name="routingKey">The routingKey<see cref="string"/></param>
        /// <param name="prefetchCount">The prefetchCount<see cref="int"/></param>
        /// <returns>The <see cref="IBus"/></returns>
        private static IBus CreateBus<T>(string exchangeType = EasyNetQ.Topology.ExchangeType.Topic, string routingKey = "", int prefetchCount = 0) where T : class
        {
           
            string strMQconnection = QueueSettingsConst.DEVCONNECTIONS;
            string strMQusername = QueueSettingsConst.DEVUSERNAME;
            string strMQpassword = QueueSettingsConst.DEVPASSWORD;

            string prefetch = prefetchCount != 0 ? $";prefetchcount={prefetchCount}" : "";
            var bus = RabbitHutch.CreateBus($"host={strMQconnection};username={strMQusername};password={strMQpassword}{prefetch}",
             x =>
             {
                 x.Register<IConventions, AttributeBasedConventions>();
                 x.EnableMessageVersioning();
             });

            string exchangeName = typeof(T).GetAttributeValue((ExchangeNameAttribute exName) => exName.ExchangeName);
            string queueName = typeof(T).GetAttributeValue((QueueNameAttribute queueAttribute) => queueAttribute.QueueName);

            IExchange exchange = bus?.Advanced?.ExchangeDeclare(exchangeName, exchangeType);
            IQueue queue = bus?.Advanced?.QueueDeclare(queueName);
            var conventions = bus?.Advanced?.Container?.Resolve<IConventions>();
            conventions.ErrorExchangeNamingConvention = info => $"Error.{queueName}";
            conventions.ErrorQueueNamingConvention = (MessageReceivedInfo info) => $"{queueName}.Error";

            bus?.Advanced?.Bind(exchange, queue, routingKey);

            return bus;
        }

        /// <summary>
        /// The CreateErrorBus
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="exchangeType">The exchangeType<see cref="string"/></param>
        /// <param name="routingKey">The routingKey<see cref="string"/></param>
        /// <param name="prefetchCount">The prefetchCount<see cref="int"/></param>
        /// <returns>The <see cref="IBus"/></returns>
        private static IBus CreateErrorBus<T>(int prefetchCount = 0) where T : class
        {
            string strMQconnection = QueueSettingsConst.DEVCONNECTIONS;
            string strMQusername = QueueSettingsConst.DEVUSERNAME;
            string strMQpassword = QueueSettingsConst.DEVPASSWORD;
           

            string prefetch = prefetchCount != 0 ? $";prefetchcount={prefetchCount}" : "";

            return RabbitHutch.CreateBus($"host={strMQconnection};username={strMQusername};password={strMQpassword}{prefetch}",
                x =>
                {
                    x.Register<IConventions, AttributeBasedConventions>();
                    x.EnableMessageVersioning();
                }
               );
        }

        #endregion
    }
}

